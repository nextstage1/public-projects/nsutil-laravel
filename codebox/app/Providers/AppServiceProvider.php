<?php

namespace App\Providers;

use Illuminate\Database\Events\QueryExecuted;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Log;
use NsUtilLaravel\Contracts\MailServiceInterface;
use NsUtilLaravel\Services\SendGridMailService;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {
        //
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        // Mail service
        $this->app->bind(MailServiceInterface::class, SendGridMailService::class);


        // Log querys
        if (env('APP_ENV') === 'local') {
            // Registre o listener para o evento QueryExecuted
            DB::listen(function (QueryExecuted $event) {
                // Adicione o nome da rota ao log
                Log::info("{$event->time}ms | {$event->sql} | " . json_encode($event->bindings));
            });
        }
    }
}
