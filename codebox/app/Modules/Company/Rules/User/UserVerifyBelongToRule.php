<?php

    namespace App\Modules\Company\Rules\User;

    use App\Generated\Modules\Company\Rules\User\AbstractUserVerifyBelongToRule;
    use Illuminate\Http\Request;

    /**
     * Class UserVerifyBelongToRule
     *
     * This class extends the generated class. Update this to resolve the requirements of this application module.
     */
    final class UserVerifyBelongToRule extends AbstractUserVerifyBelongToRule
    {
        public function __invoke(Request $request): Request
        {
            return parent::__invoke($request);
        }
    }
    