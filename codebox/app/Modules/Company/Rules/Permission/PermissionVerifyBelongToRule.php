<?php

    namespace App\Modules\Company\Rules\Permission;

    use App\Generated\Modules\Company\Rules\Permission\AbstractPermissionVerifyBelongToRule;
    use Illuminate\Http\Request;

    /**
     * Class PermissionVerifyBelongToRule
     *
     * This class extends the generated class. Update this to resolve the requirements of this application module.
     */
    final class PermissionVerifyBelongToRule extends AbstractPermissionVerifyBelongToRule
    {
        public function __invoke(Request $request): Request
        {
            return parent::__invoke($request);
        }
    }
    