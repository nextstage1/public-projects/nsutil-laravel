<?php

    namespace App\Modules\Company\Rules\Profile;

    use App\Generated\Modules\Company\Rules\Profile\AbstractProfileVerifyBelongToRule;
    use Illuminate\Http\Request;

    /**
     * Class ProfileVerifyBelongToRule
     *
     * This class extends the generated class. Update this to resolve the requirements of this application module.
     */
    final class ProfileVerifyBelongToRule extends AbstractProfileVerifyBelongToRule
    {
        public function __invoke(Request $request): Request
        {
            return parent::__invoke($request);
        }
    }
    