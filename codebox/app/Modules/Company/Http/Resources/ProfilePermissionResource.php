<?php

    namespace App\Modules\Company\Http\Resources;

    use App\Generated\Modules\Company\Http\Resources\AbstractProfilePermissionResource;

    /**
     * Class ProfilePermissionResource
     *
     * This class extends the generated class. Update this to resolve the requirements of this application module.
     */
    final class ProfilePermissionResource extends AbstractProfilePermissionResource
    {
    }    
    