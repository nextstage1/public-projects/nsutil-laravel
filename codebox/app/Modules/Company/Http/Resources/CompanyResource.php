<?php

    namespace App\Modules\Company\Http\Resources;

    use App\Generated\Modules\Company\Http\Resources\AbstractCompanyResource;

    /**
     * Class CompanyResource
     *
     * This class extends the generated class. Update this to resolve the requirements of this application module.
     */
    final class CompanyResource extends AbstractCompanyResource
    {
    }    
    