<?php

    namespace App\Modules\Company\Http\Requests;

    use App\Generated\Modules\Company\Http\Requests\AbstractUserProfileRequest;

    /**
     * Class UserProfileRequest
     *
     * This class extends the generated class. Update this to resolve the requirements of this application module.
     */
    final class UserProfileRequest extends AbstractUserProfileRequest
    {
    }    
    