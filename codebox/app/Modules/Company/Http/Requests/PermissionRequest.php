<?php

    namespace App\Modules\Company\Http\Requests;

    use App\Generated\Modules\Company\Http\Requests\AbstractPermissionRequest;

    /**
     * Class PermissionRequest
     *
     * This class extends the generated class. Update this to resolve the requirements of this application module.
     */
    final class PermissionRequest extends AbstractPermissionRequest
    {
    }    
    