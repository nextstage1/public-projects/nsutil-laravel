<?php

    namespace App\Modules\Company\Http\Requests;

    use App\Generated\Modules\Company\Http\Requests\AbstractCompanyRequest;

    /**
     * Class CompanyRequest
     *
     * This class extends the generated class. Update this to resolve the requirements of this application module.
     */
    final class CompanyRequest extends AbstractCompanyRequest
    {
    }    
    