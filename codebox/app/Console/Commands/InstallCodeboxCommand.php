<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;
use NsUtilLaravel\Builder\Codebox;
use NsUtilLaravel\Helpers\Helper;
use Symfony\Component\Console\Input\InputOption;

class InstallCodeboxCommand extends Command
{

    protected function configure()
    {
        $this->setName('app:codebox:config');
    }

    public function handle(): void
    {
        Helper::commandPrintHeader('Install Initial Files');

        Helper::shellExec('composer install --quiet');

        Codebox::config();
    }
}
