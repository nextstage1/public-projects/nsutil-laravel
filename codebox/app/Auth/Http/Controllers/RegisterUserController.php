<?php

namespace App\Auth\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Modules\Company\Models\User;
use Illuminate\Support\Facades\Hash;
use App\Modules\Company\Models\Company;
use App\Modules\Company\Models\Profile;
use App\Auth\Http\Requests\ResetRequest;
use App\Modules\Company\Models\UserProfile;
use App\Auth\Http\Requests\RegisterRequest;
use NsUtilLaravel\Contracts\MailServiceInterface;

class RegisterUserController extends Controller
{

    private MailServiceInterface $mailer;

    public function __construct(MailServiceInterface $mailer)
    {
        $this->mailer = $mailer;
    }

    private function sendToken(string $email, string $name, string $subject = 'Confirm your account')
    {
        // Enviar email do confirmação
        DB::table('password_reset_tokens')->where('email', $email)->delete();
        $token = fake()->numberBetween(100000, 999999);
        DB::table('password_reset_tokens')->insert([
            'email' => $email,
            'token' => (string) $token,
            'created_at' => Carbon::now()
        ]);
        return $this->mailer->send(
            $email,
            $name,
            $subject,
            'BODY',
            [
                'template_id' => 'd-b6b8486841ca44f8985d1d66017cfb6e',
                'template_data' => ['body' => $token, 'token' => $token]
            ]
        );
    }

    // registra um usuario
    public function register(RegisterRequest $request)
    {
        $itens = $request->only('name', 'email');
        $itens['password'] = 'PENDING';
        $user = User::create($itens);

        $ret = $this->sendToken($user->email, $user->name);

        return response()->json(
            $ret->statusCode() < 300
                ? ['message' => 'User created! Check your e-mail to validate your account']
                : ['error' => $ret->headers()],
            $ret->statusCode() ?? 400
        );
    }

    public function resendToken(Request $request)
    {

        $validatedData = $request->validate([
            'email' => 'required|email|exists:users,email'
        ]);

        $user = User::where('email', '=', $request->email)
            ->first();

        if (!$user) {
            throw new \InvalidArgumentException('Account not found', 404);
        }

        if ($user->email_verified_at !== null) {
            throw new \InvalidArgumentException('Account has already been verified', 422);
        }

        $ret = $this->sendToken($user->email, $user->name);

        return response()->json(
            $ret->statusCode() < 300
                ? ['message' => 'Token resent']
                : ['error' => $ret->headers()],
            $ret->statusCode() ?? 400
        );
    }

    // valida o código enviado por email
    public function confirm(ResetRequest $request)
    {
        // Encontrar o usuário com o email fornecido
        $user = User::where('email', $request->email)->firstOrFail();

        DB::transaction(function () use ($request, $user) {

            // Criar empresa
            $company = $this->createUserCompany($user);

            // Criar profile
            $this->createProfileRoot($user, $company);

            // Atualizar a senha do usuário
            $user->password = Hash::make($request->password);
            $user->email_verified_at = Carbon::now();
            $user->save();

            // Remover o token utilizado
            DB::table('password_reset_tokens')->where('email', $request->email)->delete();
        });

        $this->mailer->send(
            $user->email,
            $user->name,
            'Congratulations!',
            'BODY',
            [
                'template_id' => 'd-b6b8486841ca44f8985d1d66017cfb6e',
                'template_data' => ['body' => 'Your account has been activated!', 'token' => $request->token]
            ]
        );

        return response()->json(['message' => 'Account activated!'], 200);
    }

    // Cria a empresa e associa ao usuario
    public function createUserCompany(User $user)
    {
        return Company::create([
            'name' => $user->name,
            'email' => $user->email
        ]);
    }

    // Cria o profile administrador com permissão root
    public function createProfileRoot(User $user, Company $company)
    {
        $profile = Profile::create([
            'name' => 'Administrator',
            'company_id' => $company->id,
            'is_root' => true
        ]);

        UserProfile::create([
            'user_id' => $user->id,
            'profile_id' => $profile->id
        ]);

        return $profile;
    }
}
