<?php

namespace App\Auth\Http\Controllers;

use Carbon\Carbon;
use Tymon\JWTAuth\Facades\JWTAuth;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Auth\Http\Requests\LoginRequest;
use App\Modules\Company\Models\UserProfile;
use NsUtilLaravel\Contracts\MailServiceInterface;
use App\Modules\Company\Http\Resources\UserResource;
use App\Modules\Company\Http\Resources\CompanyResource;

class AuthController extends Controller
{

    private MailServiceInterface $mailer;

    public function __construct(MailServiceInterface $mailer)
    {
        $this->mailer = $mailer;
    }

    public function login(LoginRequest $request)
    {

        // check if email and password is OK
        if ($token = $request->get('token_social') || Auth::attempt($request->only('email', 'password'))) {
            // Check quantity of companies to user
            if (null !== $request->get('company_id')) {

                $userProfiles = UserProfile::with('user', 'profile')
                    ->whereHas('user', fn ($query) => $query->where('id', Auth::user()->id))
                    ->whereHas('profile', fn ($query) => $query->where('company_id', $request->get('company_id')))
                    ->get();
            } else {
                $userProfiles = UserProfile::with('user', 'profile')
                    ->whereHas('user', fn ($query) => $query->where('id', Auth::user()->id))
                    ->get();
            }

            $companiesCount = count($userProfiles);

            // User não esta em nenhuma empresa
            if ($companiesCount === 0) {
                return response()->json(['error' => null !== $request->get('company_id') ? 'Company is not enabled to user' : 'No company enabled to user'], 401);
                // User tem mais de uma empresa
            } else if ($companiesCount > 1) {
                $companies = [];
                $companies = array_map(fn ($item) => CompanyResource::make($item->profile->company), $userProfiles->all());
                return response()->json([
                    'error' => 'User has more than one associated company',
                    'content' => $companies
                ], 200);
            } else {
                // usuário tem apenas uma empresa. Adicionar company_id ao token
                $token = Auth::claims([
                    'company_id' => $userProfiles[0]->profile->company_id,
                    'profile_id' => $userProfiles[0]->profile->id
                ])
                    ->attempt($request->only('email', 'password'));
            }

            return response()->json([
                'access_token' => $token,
                'token_type' => 'bearer',
                'expires_in' => $this->getExpiresAt(),
                'user' => UserResource::make(Auth::user()),
                'company' => CompanyResource::make($userProfiles[0]->profile->company)
            ]);
        }

        return response()->json(['error' => 'The provided credentials are incorrect'], 401);
    }


    public function logout()
    {
        try {
            JWTAuth::invalidate(JWTAuth::getToken());
            return response()->json(['message' => 'Logged out successfully'], 200);
        } catch (\Tymon\JWTAuth\Exceptions\JWTException $e) {
            return response()->json(['message' => 'Failed to logout, please try again'], 500);
        }
    }

    private function getExpiresAt()
    {
        $ttl = config('jwt.ttl'); // Tempo de vida do token em minutos
        return Carbon::now()->addMinutes($ttl)->toDateTimeString();
    }
}
