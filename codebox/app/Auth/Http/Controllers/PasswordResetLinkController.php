<?php

namespace App\Auth\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Modules\Company\Models\User;
use Illuminate\Support\Facades\Hash;
use App\Auth\Http\Requests\ResetRequest;
use App\Auth\Http\Requests\ForgotRequest;
use NsUtilLaravel\Contracts\MailServiceInterface;

class PasswordResetLinkController extends Controller
{

    private MailServiceInterface $mailer;

    public function __construct(MailServiceInterface $mailer)
    {
        $this->mailer = $mailer;
    }
    /**
     * Handle an incoming password reset link request.
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    public function forgot(ForgotRequest $request): JsonResponse
    {
        $email = $request->input('email');
        $status = 400;
        $message = '';

        $user = User::where('email', $email)->firstOrFail();

        // Gerar token
        $token = fake()->numberBetween(100000, 999999);
        DB::table('password_reset_tokens')->where('email', $email)->delete();
        DB::table('password_reset_tokens')->insert([
            'email' => $email,
            'token' => (string) $token,
            'created_at' => Carbon::now()
        ]);

        // Enviar email
        $ret = $this->mailer->send(
            $user->email,
            $user->name,
            'Password recovery request',
            'BODY',
            [
                'template_id' => 'd-b6b8486841ca44f8985d1d66017cfb6e',
                'template_data' => ['body' => $token]
            ]
        );

        $out = $ret->statusCode() < 300
            ? ['message' => 'Token for password recovery sent. Check email.']
            : ['error' => $ret->headers()];

        return response()->json($out, $ret->statusCode() ?? $status);
    }

    public function reset(ResetRequest $request)
    {
        // Encontrar o usuário com o email fornecido
        $user = User::where('email', $request->email)->firstOrFail();

        // Atualizar a senha do usuário
        $user->password = Hash::make($request->password);
        $user->save();

        // Remover o token utilizado
        DB::table('password_reset_tokens')->where('email', $request->email)->delete();

        // Enviar email informando alteração de senha
        $ret = $this->mailer->send(
            $user->email,
            $user->name,
            'Password changed',
            'BODY',
            [
                'template_id' => 'd-b6b8486841ca44f8985d1d66017cfb6e',
                'template_data' => ['body' => 'Your password was change. Token: ' . $request->token]
            ]
        );

        return response()->json(['message' => 'Password successfully updated'], 200);
    }
}
