<?php

namespace App\Auth\Http\Middlewares;

use App\Modules\Company\Models\Profile;
use Closure;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Facades\JWTAuth;

class CheckToken
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle(Request $request, Closure $next): Response
    {
        if (env('APP_ENV') === 'local') {
            \Illuminate\Support\Facades\Log::info('---> ' . $request->route()->getName());
        }

        $token = $request->header('Authorization');

        if (!$token) {
            return response()->json(['error' => 'Token not provided'], 400);
        }

        try {
            $user = JWTAuth::parseToken()->authenticate();
            // Acrescentar os extras vindos do JWT
            $payload = JWTAuth::getPayload($token);
            $request->merge([
                'jwt_company_id' => $payload->get('company_id'),
                'jwt_profile_id' => $payload->get('profile_id'),
                'company_id' => $payload->get('company_id')
            ]);
            return $next($request);
        } catch (JWTException $e) {
            return response()->json(
                [
                    'error' => (getenv('APP_ENV') === 'local' ? [
                        'message' => $e->getMessage(),
                        'token' => $token
                    ] : 'Token invalid')
                ],
                400
            );
        }
    }
}
