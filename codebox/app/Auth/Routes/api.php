<?php

use Illuminate\Support\Facades\Route;


$limitSeconds = env('APP_ENV') === 'testing' ? 120 : 10;
// Auth modules
Route::middleware(["throttle:{$limitSeconds},1"])->group(function () {

    Route::get('healthcheck', fn () => response()->json('healthy', 200))->name('app.healthcheck');

    Route::prefix('auth')
        ->group(function () {
            Route::post('login', [\App\Auth\Http\Controllers\AuthController::class, 'login'])->name('auth.login');
            Route::post('logout', [\App\Auth\Http\Controllers\AuthController::class, 'logout'])->name('auth.logout');
            Route::post('forgot-password', [\App\Auth\Http\Controllers\PasswordResetLinkController::class, 'forgot'])->name('auth.forgot');
            Route::post('reset-password', [\App\Auth\Http\Controllers\PasswordResetLinkController::class, 'reset'])->name('auth.reset');

            // Google
            Route::get('google', [\App\Auth\Http\Controllers\SocialAuthController::class, 'redirectToGoogle'])->name('auth.google');
            Route::get('google/callback', [\App\Auth\Http\Controllers\SocialAuthController::class, 'handleGoogleCallback'])->name('auth.google.callback');
        });


    // Create account
    Route::prefix('signup')
        ->group(function () {
            Route::post('', [\App\Auth\Http\Controllers\RegisterUserController::class, 'register'])->name('signup.register');
            Route::post('resend', [\App\Auth\Http\Controllers\RegisterUserController::class, 'resendToken'])->name('signup.resend');
            Route::post('confirm', [\App\Auth\Http\Controllers\RegisterUserController::class, 'confirm'])->name('signup.confirm');
        });
});
