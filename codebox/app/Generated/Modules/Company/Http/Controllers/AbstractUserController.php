<?php

    namespace App\Generated\Modules\Company\Http\Controllers;
    
    use App\Modules\Company\Http\Requests\UserRequest;
    use App\Modules\Company\Http\Resources\UserResource;
    use App\Modules\Company\Models\User;
    use Illuminate\Http\Request;

    /**
     * UserController
     * 
     * This class is responsible for handling requests related to the User model.
     * 
     * @package App\Modules\Company\Http\Controllers
     */ 
    abstract class AbstractUserController extends \App\Http\Controllers\Controller
    {
        /**
         * Conditions to filter by.
         *
         * @var array
         */
        protected array $conditions = [];

        /**
         * Sortable fields.
         *
         * @var array
         */
        protected array $sortable = [
            'id', 'created_at', 'updated_at', 'name', 'email', 'email_verified_at', 'password'
        ];

        public function __construct()
        {
            
        }
    
        /**
         * Initialize conditions for filtering.
         *
         * @param UserRequest $request
         * @return void
         */
        protected function initCondition(UserRequest $request)
        {
            $this->conditions = [];

            $this->conditions['id'] = (int) \Illuminate\Support\Facades\Auth::user()->id;
        }

        /**
         * Read a specific document type.
         *
         * @param int $id
         * @return User
         * @throws \Illuminate\Database\Eloquent\ModelNotFoundException
         */
        protected function read(int $id) {
            return User::where('users.id', (int) $id)
                        ->select('users.*')
            ->where($this->conditions)
            ->firstOrFail();
        }

        /**
         * Get a list of document types.
         *
         * @param UserRequest $request
         * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
         */
        public function index(UserRequest $request)
        {
            $this->initCondition($request);
            $list =  User::select('users.*')
                                ->where($this->conditions);
            
    
            // Limit per page. Default 30. Max 100
            $limit = $request->get('per_page') ?? env('LIMIT_PER_PAGE') ?? 30;

            // Order
            if ($request->get('order')) {
                if (!in_array($request->get('order'), $this->sortable)) {
                    return response()->json(['error' => 'The requested field is not enabled for sorting' ], 400);
                }
                $request->get('sort') === 'desc'
                    ? $list->orderByDesc($request->get('order'))
                    : $list->orderBy($request->get('order'));
            }
    
            return UserResource::collection(
                $list->paginate($limit > 100 ? 100 : $limit)
            );
        }
    
        /**
         * Store a new document type.
         *
         * @param UserRequest $request
         * @return UserResource
         */
        public function store(UserRequest $request)
        {
            $this->initCondition($request);
            $item = User::create($request->all());
            

            return (new UserResource($item))
            ->response()
            ->setStatusCode(201);
        }
    
        /**
         * Display the specified User.
         *
         * @param \App\Modules\Company\Http\Requests\UserRequest  $request
         * @param int  $id
         * @return \Illuminate\Http\Response
         */
        public function show(UserRequest $request)
        {
            $this->initCondition($request);
            $item = $this->read((int) $request->route('user'));
            

            return new UserResource($item);
        }
        
        /**
         * Update the specified User in storage.
         *
         * @param \App\Modules\Company\Http\Requests\UserRequest  $request
         * @param int  $id
         * @return \Illuminate\Http\Response
         */
        public function update(UserRequest $request)
        {
            $this->initCondition($request);
            $item = $this->read((int) $request->route('user'));
            $item->update($request->all());
            

            return new UserResource($item);
        }
    
        /**
         * Remove the specified User from storage.
         *
         * @param \App\Modules\Company\Http\Requests\UserRequest $request
         * @param int $id
         * @return \Illuminate\Http\Response
         */
        public function destroy(UserRequest $request)
        {
            $this->initCondition($request);
            $item = $this->read((int) $request->route('user'))
                ->delete();

            return response()->json([], 204);
        }
    }    
    