<?php

    namespace App\Generated\Modules\Company\Http\Controllers;
    
    use App\Modules\Company\Http\Requests\ProfileRequest;
    use App\Modules\Company\Http\Resources\ProfileResource;
    use App\Modules\Company\Models\Profile;
    use Illuminate\Http\Request;

    /**
     * ProfileController
     * 
     * This class is responsible for handling requests related to the Profile model.
     * 
     * @package App\Modules\Company\Http\Controllers
     */ 
    abstract class AbstractProfileController extends \App\Http\Controllers\Controller
    {
        /**
         * Conditions to filter by.
         *
         * @var array
         */
        protected array $conditions = [];

        /**
         * Sortable fields.
         *
         * @var array
         */
        protected array $sortable = [
            'id', 'created_at', 'updated_at', 'name'
        ];

        public function __construct()
        {
            
        }
    
        /**
         * Initialize conditions for filtering.
         *
         * @param ProfileRequest $request
         * @return void
         */
        protected function initCondition(ProfileRequest $request)
        {
            $this->conditions = [];

            $this->conditions['company_id'] = (int) $request->get('jwt_company_id');
        }

        /**
         * Read a specific document type.
         *
         * @param int $id
         * @return Profile
         * @throws \Illuminate\Database\Eloquent\ModelNotFoundException
         */
        protected function read(int $id) {
            return Profile::where('profiles.id', (int) $id)
                        ->select('profiles.*')
            ->where($this->conditions)
            ->firstOrFail();
        }

        /**
         * Get a list of document types.
         *
         * @param ProfileRequest $request
         * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
         */
        public function index(ProfileRequest $request)
        {
            $this->initCondition($request);
            $list =  Profile::select('profiles.*')
                                ->where($this->conditions);
            
    
            // Limit per page. Default 30. Max 100
            $limit = $request->get('per_page') ?? env('LIMIT_PER_PAGE') ?? 30;

            // Order
            if ($request->get('order')) {
                if (!in_array($request->get('order'), $this->sortable)) {
                    return response()->json(['error' => 'The requested field is not enabled for sorting' ], 400);
                }
                $request->get('sort') === 'desc'
                    ? $list->orderByDesc($request->get('order'))
                    : $list->orderBy($request->get('order'));
            }
    
            return ProfileResource::collection(
                $list->paginate($limit > 100 ? 100 : $limit)
            );
        }
    
        /**
         * Store a new document type.
         *
         * @param ProfileRequest $request
         * @return ProfileResource
         */
        public function store(ProfileRequest $request)
        {
            $this->initCondition($request);
            $item = Profile::create($request->all());
            

            return (new ProfileResource($item))
            ->response()
            ->setStatusCode(201);
        }
    
        /**
         * Display the specified Profile.
         *
         * @param \App\Modules\Company\Http\Requests\ProfileRequest  $request
         * @param int  $id
         * @return \Illuminate\Http\Response
         */
        public function show(ProfileRequest $request)
        {
            $this->initCondition($request);
            $item = $this->read((int) $request->route('profile'));
            

            return new ProfileResource($item);
        }
        
        /**
         * Update the specified Profile in storage.
         *
         * @param \App\Modules\Company\Http\Requests\ProfileRequest  $request
         * @param int  $id
         * @return \Illuminate\Http\Response
         */
        public function update(ProfileRequest $request)
        {
            $this->initCondition($request);
            $item = $this->read((int) $request->route('profile'));
            $item->update($request->all());
            

            return new ProfileResource($item);
        }
    
        /**
         * Remove the specified Profile from storage.
         *
         * @param \App\Modules\Company\Http\Requests\ProfileRequest $request
         * @param int $id
         * @return \Illuminate\Http\Response
         */
        public function destroy(ProfileRequest $request)
        {
            $this->initCondition($request);
            $item = $this->read((int) $request->route('profile'))
                ->delete();

            return response()->json([], 204);
        }
    }    
    