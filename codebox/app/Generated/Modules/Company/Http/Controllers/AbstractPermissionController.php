<?php

    namespace App\Generated\Modules\Company\Http\Controllers;
    
    use App\Modules\Company\Http\Requests\PermissionRequest;
    use App\Modules\Company\Http\Resources\PermissionResource;
    use App\Modules\Company\Models\Permission;
    use Illuminate\Http\Request;

    /**
     * PermissionController
     * 
     * This class is responsible for handling requests related to the Permission model.
     * 
     * @package App\Modules\Company\Http\Controllers
     */ 
    abstract class AbstractPermissionController extends \App\Http\Controllers\Controller
    {
        /**
         * Conditions to filter by.
         *
         * @var array
         */
        protected array $conditions = [];

        /**
         * Sortable fields.
         *
         * @var array
         */
        protected array $sortable = [
            'id', 'created_at', 'updated_at', 'group', 'route', 'verb'
        ];

        public function __construct()
        {
            //Table not linked to a company. API access not available.

                    if (!\Illuminate\Support\Facades\App::runningInConsole()) {
                    throw new \Illuminate\Validation\UnauthorizedException('Sorry, you don\'t have permission to access this resource. (Error. PNED300)', 403);

                    }
        }
    
        /**
         * Initialize conditions for filtering.
         *
         * @param PermissionRequest $request
         * @return void
         */
        protected function initCondition(PermissionRequest $request)
        {
            $this->conditions = [];

            
        }

        /**
         * Read a specific document type.
         *
         * @param int $id
         * @return Permission
         * @throws \Illuminate\Database\Eloquent\ModelNotFoundException
         */
        protected function read(int $id) {
            return Permission::where('permissions.id', (int) $id)
                        ->select('permissions.*')
            ->where($this->conditions)
            ->firstOrFail();
        }

        /**
         * Get a list of document types.
         *
         * @param PermissionRequest $request
         * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
         */
        public function index(PermissionRequest $request)
        {
            $this->initCondition($request);
            $list =  Permission::select('permissions.*')
                                ->where($this->conditions);
            
    
            // Limit per page. Default 30. Max 100
            $limit = $request->get('per_page') ?? env('LIMIT_PER_PAGE') ?? 30;

            // Order
            if ($request->get('order')) {
                if (!in_array($request->get('order'), $this->sortable)) {
                    return response()->json(['error' => 'The requested field is not enabled for sorting' ], 400);
                }
                $request->get('sort') === 'desc'
                    ? $list->orderByDesc($request->get('order'))
                    : $list->orderBy($request->get('order'));
            }
    
            return PermissionResource::collection(
                $list->paginate($limit > 100 ? 100 : $limit)
            );
        }
    
        /**
         * Store a new document type.
         *
         * @param PermissionRequest $request
         * @return PermissionResource
         */
        public function store(PermissionRequest $request)
        {
            $this->initCondition($request);
            $item = Permission::create($request->all());
            

            return (new PermissionResource($item))
            ->response()
            ->setStatusCode(201);
        }
    
        /**
         * Display the specified Permission.
         *
         * @param \App\Modules\Company\Http\Requests\PermissionRequest  $request
         * @param int  $id
         * @return \Illuminate\Http\Response
         */
        public function show(PermissionRequest $request)
        {
            $this->initCondition($request);
            $item = $this->read((int) $request->route('permission'));
            

            return new PermissionResource($item);
        }
        
        /**
         * Update the specified Permission in storage.
         *
         * @param \App\Modules\Company\Http\Requests\PermissionRequest  $request
         * @param int  $id
         * @return \Illuminate\Http\Response
         */
        public function update(PermissionRequest $request)
        {
            $this->initCondition($request);
            $item = $this->read((int) $request->route('permission'));
            $item->update($request->all());
            

            return new PermissionResource($item);
        }
    
        /**
         * Remove the specified Permission from storage.
         *
         * @param \App\Modules\Company\Http\Requests\PermissionRequest $request
         * @param int $id
         * @return \Illuminate\Http\Response
         */
        public function destroy(PermissionRequest $request)
        {
            $this->initCondition($request);
            $item = $this->read((int) $request->route('permission'))
                ->delete();

            return response()->json([], 204);
        }
    }    
    