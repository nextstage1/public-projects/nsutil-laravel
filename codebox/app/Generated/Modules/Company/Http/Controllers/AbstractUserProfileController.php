<?php

    namespace App\Generated\Modules\Company\Http\Controllers;
    
    use App\Modules\Company\Http\Requests\UserProfileRequest;
    use App\Modules\Company\Http\Resources\UserProfileResource;
    use App\Modules\Company\Models\UserProfile;
    use Illuminate\Http\Request;

    /**
     * UserProfileController
     * 
     * This class is responsible for handling requests related to the UserProfile model.
     * 
     * @package App\Modules\Company\Http\Controllers
     */ 
    abstract class AbstractUserProfileController extends \App\Http\Controllers\Controller
    {
        /**
         * Conditions to filter by.
         *
         * @var array
         */
        protected array $conditions = [];

        /**
         * Sortable fields.
         *
         * @var array
         */
        protected array $sortable = [
            'id', 'created_at', 'updated_at'
        ];

        public function __construct()
        {
            
        }
    
        /**
         * Initialize conditions for filtering.
         *
         * @param UserProfileRequest $request
         * @return void
         */
        protected function initCondition(UserProfileRequest $request)
        {
            $this->conditions = [];

            $this->conditions['profiles.company_id'] = (int) $request->get('jwt_company_id');

if (null !== $request->route('profile')) {$this->conditions['profiles.id'] = (int) $request->route('profile');}
        }

        /**
         * Read a specific document type.
         *
         * @param int $id
         * @return UserProfile
         * @throws \Illuminate\Database\Eloquent\ModelNotFoundException
         */
        protected function read(int $id) {
            return UserProfile::where('user_profile.id', (int) $id)
            ->join('profiles', 'profiles.id', '=', 'user_profile.profile_id')
->join('users', 'users.id', '=', 'user_profile.user_id')
            ->select('user_profile.*')
            ->where($this->conditions)
            ->firstOrFail();
        }

        /**
         * Get a list of document types.
         *
         * @param UserProfileRequest $request
         * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
         */
        public function index(UserProfileRequest $request)
        {
            $this->initCondition($request);
            $list =  UserProfile::select('user_profile.*')
                ->join('profiles', 'profiles.id', '=', 'user_profile.profile_id')
->join('users', 'users.id', '=', 'user_profile.user_id')
                ->where($this->conditions);
            $list->with('user','profile');
    
            // Limit per page. Default 30. Max 100
            $limit = $request->get('per_page') ?? env('LIMIT_PER_PAGE') ?? 30;

            // Order
            if ($request->get('order')) {
                if (!in_array($request->get('order'), $this->sortable)) {
                    return response()->json(['error' => 'The requested field is not enabled for sorting' ], 400);
                }
                $request->get('sort') === 'desc'
                    ? $list->orderByDesc($request->get('order'))
                    : $list->orderBy($request->get('order'));
            }
    
            return UserProfileResource::collection(
                $list->paginate($limit > 100 ? 100 : $limit)
            );
        }
    
        /**
         * Store a new document type.
         *
         * @param UserProfileRequest $request
         * @return UserProfileResource
         */
        public function store(UserProfileRequest $request)
        {
            $this->initCondition($request);
            $item = UserProfile::create($request->all());
            $item->load('user','profile');

            return (new UserProfileResource($item))
            ->response()
            ->setStatusCode(201);
        }
    
        /**
         * Display the specified UserProfile.
         *
         * @param \App\Modules\Company\Http\Requests\UserProfileRequest  $request
         * @param int  $id
         * @return \Illuminate\Http\Response
         */
        public function show(UserProfileRequest $request)
        {
            $this->initCondition($request);
            $item = $this->read((int) $request->route('user_profile'));
            $item->load('user','profile');

            return new UserProfileResource($item);
        }
        
        /**
         * Update the specified UserProfile in storage.
         *
         * @param \App\Modules\Company\Http\Requests\UserProfileRequest  $request
         * @param int  $id
         * @return \Illuminate\Http\Response
         */
        public function update(UserProfileRequest $request)
        {
            $this->initCondition($request);
            $item = $this->read((int) $request->route('user_profile'));
            $item->update($request->all());
            $item->load('user','profile');

            return new UserProfileResource($item);
        }
    
        /**
         * Remove the specified UserProfile from storage.
         *
         * @param \App\Modules\Company\Http\Requests\UserProfileRequest $request
         * @param int $id
         * @return \Illuminate\Http\Response
         */
        public function destroy(UserProfileRequest $request)
        {
            $this->initCondition($request);
            $item = $this->read((int) $request->route('user_profile'))
                ->delete();

            return response()->json([], 204);
        }
    }    
    