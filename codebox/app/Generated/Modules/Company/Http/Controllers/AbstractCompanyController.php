<?php

    namespace App\Generated\Modules\Company\Http\Controllers;
    
    use App\Modules\Company\Http\Requests\CompanyRequest;
    use App\Modules\Company\Http\Resources\CompanyResource;
    use App\Modules\Company\Models\Company;
    use Illuminate\Http\Request;

    /**
     * CompanyController
     * 
     * This class is responsible for handling requests related to the Company model.
     * 
     * @package App\Modules\Company\Http\Controllers
     */ 
    abstract class AbstractCompanyController extends \App\Http\Controllers\Controller
    {
        /**
         * Conditions to filter by.
         *
         * @var array
         */
        protected array $conditions = [];

        /**
         * Sortable fields.
         *
         * @var array
         */
        protected array $sortable = [
            'id', 'created_at', 'updated_at', 'name', 'email'
        ];

        public function __construct()
        {
            
        }
    
        /**
         * Initialize conditions for filtering.
         *
         * @param CompanyRequest $request
         * @return void
         */
        protected function initCondition(CompanyRequest $request)
        {
            $this->conditions = [];

            $this->conditions['id'] = (int) $request->get('jwt_company_id');
        }

        /**
         * Read a specific document type.
         *
         * @param int $id
         * @return Company
         * @throws \Illuminate\Database\Eloquent\ModelNotFoundException
         */
        protected function read(int $id) {
            return Company::where('companies.id', (int) $id)
                        ->select('companies.*')
            ->where($this->conditions)
            ->firstOrFail();
        }

        /**
         * Get a list of document types.
         *
         * @param CompanyRequest $request
         * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
         */
        public function index(CompanyRequest $request)
        {
            $this->initCondition($request);
            $list =  Company::select('companies.*')
                                ->where($this->conditions);
            
    
            // Limit per page. Default 30. Max 100
            $limit = $request->get('per_page') ?? env('LIMIT_PER_PAGE') ?? 30;

            // Order
            if ($request->get('order')) {
                if (!in_array($request->get('order'), $this->sortable)) {
                    return response()->json(['error' => 'The requested field is not enabled for sorting' ], 400);
                }
                $request->get('sort') === 'desc'
                    ? $list->orderByDesc($request->get('order'))
                    : $list->orderBy($request->get('order'));
            }
    
            return CompanyResource::collection(
                $list->paginate($limit > 100 ? 100 : $limit)
            );
        }
    
        /**
         * Store a new document type.
         *
         * @param CompanyRequest $request
         * @return CompanyResource
         */
        public function store(CompanyRequest $request)
        {
            $this->initCondition($request);
            $item = Company::create($request->all());
            

            return (new CompanyResource($item))
            ->response()
            ->setStatusCode(201);
        }
    
        /**
         * Display the specified Company.
         *
         * @param \App\Modules\Company\Http\Requests\CompanyRequest  $request
         * @param int  $id
         * @return \Illuminate\Http\Response
         */
        public function show(CompanyRequest $request)
        {
            $this->initCondition($request);
            $item = $this->read((int) $request->route('company'));
            

            return new CompanyResource($item);
        }
        
        /**
         * Update the specified Company in storage.
         *
         * @param \App\Modules\Company\Http\Requests\CompanyRequest  $request
         * @param int  $id
         * @return \Illuminate\Http\Response
         */
        public function update(CompanyRequest $request)
        {
            $this->initCondition($request);
            $item = $this->read((int) $request->route('company'));
            $item->update($request->all());
            

            return new CompanyResource($item);
        }
    
        /**
         * Remove the specified Company from storage.
         *
         * @param \App\Modules\Company\Http\Requests\CompanyRequest $request
         * @param int $id
         * @return \Illuminate\Http\Response
         */
        public function destroy(CompanyRequest $request)
        {
            $this->initCondition($request);
            $item = $this->read((int) $request->route('company'))
                ->delete();

            return response()->json([], 204);
        }
    }    
    