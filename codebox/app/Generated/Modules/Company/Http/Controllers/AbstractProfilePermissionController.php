<?php

    namespace App\Generated\Modules\Company\Http\Controllers;
    
    use App\Modules\Company\Http\Requests\ProfilePermissionRequest;
    use App\Modules\Company\Http\Resources\ProfilePermissionResource;
    use App\Modules\Company\Models\ProfilePermission;
    use Illuminate\Http\Request;

    /**
     * ProfilePermissionController
     * 
     * This class is responsible for handling requests related to the ProfilePermission model.
     * 
     * @package App\Modules\Company\Http\Controllers
     */ 
    abstract class AbstractProfilePermissionController extends \App\Http\Controllers\Controller
    {
        /**
         * Conditions to filter by.
         *
         * @var array
         */
        protected array $conditions = [];

        /**
         * Sortable fields.
         *
         * @var array
         */
        protected array $sortable = [
            'id', 'created_at', 'updated_at'
        ];

        public function __construct()
        {
            
        }
    
        /**
         * Initialize conditions for filtering.
         *
         * @param ProfilePermissionRequest $request
         * @return void
         */
        protected function initCondition(ProfilePermissionRequest $request)
        {
            $this->conditions = [];

            $this->conditions['profiles.company_id'] = (int) $request->get('jwt_company_id');

if (null !== $request->route('profile')) {$this->conditions['profiles.id'] = (int) $request->route('profile');}
        }

        /**
         * Read a specific document type.
         *
         * @param int $id
         * @return ProfilePermission
         * @throws \Illuminate\Database\Eloquent\ModelNotFoundException
         */
        protected function read(int $id) {
            return ProfilePermission::where('profile_permissions.id', (int) $id)
            ->join('profiles', 'profiles.id', '=', 'profile_permissions.profile_id')
->join('permissions', 'permissions.id', '=', 'profile_permissions.permission_id')
            ->select('profile_permissions.*')
            ->where($this->conditions)
            ->firstOrFail();
        }

        /**
         * Get a list of document types.
         *
         * @param ProfilePermissionRequest $request
         * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
         */
        public function index(ProfilePermissionRequest $request)
        {
            $this->initCondition($request);
            $list =  ProfilePermission::select('profile_permissions.*')
                ->join('profiles', 'profiles.id', '=', 'profile_permissions.profile_id')
->join('permissions', 'permissions.id', '=', 'profile_permissions.permission_id')
                ->where($this->conditions);
            $list->with('permission','profile');
    
            // Limit per page. Default 30. Max 100
            $limit = $request->get('per_page') ?? env('LIMIT_PER_PAGE') ?? 30;

            // Order
            if ($request->get('order')) {
                if (!in_array($request->get('order'), $this->sortable)) {
                    return response()->json(['error' => 'The requested field is not enabled for sorting' ], 400);
                }
                $request->get('sort') === 'desc'
                    ? $list->orderByDesc($request->get('order'))
                    : $list->orderBy($request->get('order'));
            }
    
            return ProfilePermissionResource::collection(
                $list->paginate($limit > 100 ? 100 : $limit)
            );
        }
    
        /**
         * Store a new document type.
         *
         * @param ProfilePermissionRequest $request
         * @return ProfilePermissionResource
         */
        public function store(ProfilePermissionRequest $request)
        {
            $this->initCondition($request);
            $item = ProfilePermission::create($request->all());
            $item->load('permission','profile');

            return (new ProfilePermissionResource($item))
            ->response()
            ->setStatusCode(201);
        }
    
        /**
         * Display the specified ProfilePermission.
         *
         * @param \App\Modules\Company\Http\Requests\ProfilePermissionRequest  $request
         * @param int  $id
         * @return \Illuminate\Http\Response
         */
        public function show(ProfilePermissionRequest $request)
        {
            $this->initCondition($request);
            $item = $this->read((int) $request->route('profile_permission'));
            $item->load('permission','profile');

            return new ProfilePermissionResource($item);
        }
        
        /**
         * Update the specified ProfilePermission in storage.
         *
         * @param \App\Modules\Company\Http\Requests\ProfilePermissionRequest  $request
         * @param int  $id
         * @return \Illuminate\Http\Response
         */
        public function update(ProfilePermissionRequest $request)
        {
            $this->initCondition($request);
            $item = $this->read((int) $request->route('profile_permission'));
            $item->update($request->all());
            $item->load('permission','profile');

            return new ProfilePermissionResource($item);
        }
    
        /**
         * Remove the specified ProfilePermission from storage.
         *
         * @param \App\Modules\Company\Http\Requests\ProfilePermissionRequest $request
         * @param int $id
         * @return \Illuminate\Http\Response
         */
        public function destroy(ProfilePermissionRequest $request)
        {
            $this->initCondition($request);
            $item = $this->read((int) $request->route('profile_permission'))
                ->delete();

            return response()->json([], 204);
        }
    }    
    