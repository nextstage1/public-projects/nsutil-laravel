<?php

    namespace App\Generated\Modules\Company\Http\Resources;
    
    use Illuminate\Http\Request;
    use Illuminate\Http\Resources\Json\JsonResource;
    
    abstract class AbstractCompanyResource extends JsonResource
    {
        /**
         * Transform the resource into an array.
         *
         * @return array<string, mixed>
         */
        public function toArray(Request $request): array
        {
            $data = [];
            if ($this->resource instanceof \Illuminate\Database\Eloquent\Model) {
                
                $data = ['id'=>$this->id,
'name'=>$this->name,
'email'=>$this->email];
            }

            return $data;

        }
    }
    