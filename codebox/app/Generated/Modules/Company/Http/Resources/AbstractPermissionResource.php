<?php

    namespace App\Generated\Modules\Company\Http\Resources;
    
    use Illuminate\Http\Request;
    use Illuminate\Http\Resources\Json\JsonResource;
    
    abstract class AbstractPermissionResource extends JsonResource
    {
        /**
         * Transform the resource into an array.
         *
         * @return array<string, mixed>
         */
        public function toArray(Request $request): array
        {
            $data = [];
            if ($this->resource instanceof \Illuminate\Database\Eloquent\Model) {
                
                $data = ['id'=>$this->id,
'group'=>$this->group,
'route'=>$this->route,
'verb'=>$this->verb];
            }

            return $data;

        }
    }
    