<?php

    namespace  App\Generated\Modules\Company\Http\Requests;
    
    
    abstract class AbstractProfileRequest extends \NsUtilLaravel\Http\Request\DefaultRequest
    {
        public function authorize()
        {
            return true;
        }
    
        public function rules()
        {
            return parent::rulesDefault([
                'name'=>'required|string|max:100',
'is_root'=>'required|boolean',
'company_id'=>'required|integer|exists:companies,id'
            ]);
        }
    
        public function messages()
        {
            return [
                'name.max'=>__tr('profile.name').': '.__tr('Max characters exceeded') . ' (:max)',
'name.required'=>__tr('profile.name').': '.__tr('The field is required'),
'name.string'=>__tr('profile.name').': '.__tr('Must be a string'),
'is_root.required'=>__tr('profile.is_root').': '.__tr('The field is required'),
'is_root.boolean'=>__tr('profile.is_root').': '.__tr('Must be a boolean'),
'company_id.exists'=>__tr('company.company_id').': '.__tr('The value is invalid'),
'company_id.required'=>__tr('company.company_id').': '.__tr('The field is required')                
            ];
        }

        
    }
    