<?php

    namespace  App\Generated\Modules\Company\Http\Requests;
    
    
    abstract class AbstractProfilePermissionRequest extends \NsUtilLaravel\Http\Request\DefaultRequest
    {
        public function authorize()
        {
            return true;
        }
    
        public function rules()
        {
            return parent::rulesDefault([
                'permission_id'=>'required|integer|exists:permissions,id',
'profile_id'=>'required|integer|exists:profiles,id'
            ]);
        }
    
        public function messages()
        {
            return [
                'permission_id.exists'=>__tr('permission.permission_id').': '.__tr('The value is invalid'),
'permission_id.required'=>__tr('permission.permission_id').': '.__tr('The field is required'),
'profile_id.exists'=>__tr('profile.profile_id').': '.__tr('The value is invalid'),
'profile_id.required'=>__tr('profile.profile_id').': '.__tr('The field is required')                
            ];
        }

        
    }
    