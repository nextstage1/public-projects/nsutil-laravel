<?php

    namespace  App\Generated\Modules\Company\Http\Requests;
    
    
    abstract class AbstractCompanyRequest extends \NsUtilLaravel\Http\Request\DefaultRequest
    {
        public function authorize()
        {
            return true;
        }
    
        public function rules()
        {
            return parent::rulesDefault([
                'name'=>'required|string|max:255',
'email'=>'required|string|email'
            ]);
        }
    
        public function messages()
        {
            return [
                'name.max'=>__tr('company.name').': '.__tr('Max characters exceeded') . ' (:max)',
'name.required'=>__tr('company.name').': '.__tr('The field is required'),
'name.string'=>__tr('company.name').': '.__tr('Must be a string'),
'email.email'=>__tr('company.email').': '.__tr('Please enter a valid email address'),
'email.required'=>__tr('company.email').': '.__tr('The field is required'),
'email.string'=>__tr('company.email').': '.__tr('Must be a string')                
            ];
        }

        
    }
    