<?php

    namespace  App\Generated\Modules\Company\Http\Requests;
    
    
    abstract class AbstractUserProfileRequest extends \NsUtilLaravel\Http\Request\DefaultRequest
    {
        public function authorize()
        {
            return true;
        }
    
        public function rules()
        {
            return parent::rulesDefault([
                'user_id'=>'required|integer|exists:users,id',
'profile_id'=>'required|integer|exists:profiles,id'
            ]);
        }
    
        public function messages()
        {
            return [
                'user_id.exists'=>__tr('user.user_id').': '.__tr('The value is invalid'),
'user_id.required'=>__tr('user.user_id').': '.__tr('The field is required'),
'profile_id.exists'=>__tr('profile.profile_id').': '.__tr('The value is invalid'),
'profile_id.required'=>__tr('profile.profile_id').': '.__tr('The field is required')                
            ];
        }

        
    }
    