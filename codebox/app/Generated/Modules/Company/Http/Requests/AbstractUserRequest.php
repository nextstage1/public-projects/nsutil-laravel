<?php

    namespace  App\Generated\Modules\Company\Http\Requests;
    
    
    abstract class AbstractUserRequest extends \NsUtilLaravel\Http\Request\DefaultRequest
    {
        public function authorize()
        {
            return true;
        }
    
        public function rules()
        {
            return parent::rulesDefault([
                'name'=>'required|string|max:255',
'email'=>'required|string|email',
'email_verified_at'=>'nullable|date|email',
'password'=>'required|string|max:255'
            ]);
        }
    
        public function messages()
        {
            return [
                'name.max'=>__tr('user.name').': '.__tr('Max characters exceeded') . ' (:max)',
'name.required'=>__tr('user.name').': '.__tr('The field is required'),
'name.string'=>__tr('user.name').': '.__tr('Must be a string'),
'email.email'=>__tr('user.email').': '.__tr('Please enter a valid email address'),
'email.required'=>__tr('user.email').': '.__tr('The field is required'),
'email.string'=>__tr('user.email').': '.__tr('Must be a string'),
'email_verified_at.email'=>__tr('user.email_verified_at').': '.__tr('Please enter a valid email address'),
'email_verified_at.required'=>__tr('user.email_verified_at').': '.__tr('The field is required'),
'email_verified_at.timestamp'=>__tr('user.email_verified_at').': '.__tr('Must be a timestamp'),
'password.max'=>__tr('user.password').': '.__tr('Max characters exceeded') . ' (:max)',
'password.required'=>__tr('user.password').': '.__tr('The field is required'),
'password.string'=>__tr('user.password').': '.__tr('Must be a string')                
            ];
        }

        
    }
    