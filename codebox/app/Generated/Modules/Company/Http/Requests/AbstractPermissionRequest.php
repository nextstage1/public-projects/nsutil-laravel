<?php

    namespace  App\Generated\Modules\Company\Http\Requests;
    
    
    abstract class AbstractPermissionRequest extends \NsUtilLaravel\Http\Request\DefaultRequest
    {
        public function authorize()
        {
            return true;
        }
    
        public function rules()
        {
            return parent::rulesDefault([
                'group'=>'required|string|max:50',
'route'=>'required|string|max:50',
'verb'=>'required|string|max:50'
            ]);
        }
    
        public function messages()
        {
            return [
                'group.max'=>__tr('permission.group').': '.__tr('Max characters exceeded') . ' (:max)',
'group.required'=>__tr('permission.group').': '.__tr('The field is required'),
'group.string'=>__tr('permission.group').': '.__tr('Must be a string'),
'route.max'=>__tr('permission.route').': '.__tr('Max characters exceeded') . ' (:max)',
'route.required'=>__tr('permission.route').': '.__tr('The field is required'),
'route.string'=>__tr('permission.route').': '.__tr('Must be a string'),
'verb.max'=>__tr('permission.verb').': '.__tr('Max characters exceeded') . ' (:max)',
'verb.required'=>__tr('permission.verb').': '.__tr('The field is required'),
'verb.string'=>__tr('permission.verb').': '.__tr('Must be a string')                
            ];
        }

        
    }
    