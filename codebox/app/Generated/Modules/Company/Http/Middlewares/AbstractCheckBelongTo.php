<?php

        namespace App\Generated\Modules\Company\Http\Middlewares;
        
        use Closure;
        use Illuminate\Http\Request;
        use NsUtilLaravel\Helpers\Helper;
        use Illuminate\Support\Facades\File;
        use Illuminate\Support\Facades\Cache;
        
        abstract class AbstractCheckBelongTo
        {
            /**
             * Handle an incoming request.
             *
             * @param  \Illuminate\Http\Request  $request
             * @param  \Closure  $next
             * @return mixed
             */
            public function handle(Request $request, Closure $next)
            {
                // Check if the HTTP method is POST or PUT
                //if (in_array($request->method(),  ['POST', 'PUT'])) {
    
                    $routeName = $request->route()->getName();
                    $cacheKey = "CheckBelongTo-$routeName";
                    $rules = Cache::rememberForever($cacheKey, function () use ($routeName) {
                        $parts = explode('.', $routeName);
                        array_pop($parts);
                        $rules = [];
                        foreach ($parts as $route) {
                            $modelName = ucwords(Helper::name2CamelCase($route));

                            // Build the name of the verification rule based on the route name
                            $model = "\\App\\Modules\\Company\\Rules\\$modelName";

                                // Procurar por todos os arquivos de rules desta model
                                $files = File::files(base_path() . '/app/Modules/Company/Rules/' . $modelName);
                                foreach ($files as $file) {
                                    $filename = pathinfo($file, PATHINFO_FILENAME);
                                    $rule = $model . '\\'. $filename;
                                    // Call the verification rule to check if the user has access to the resource
                                    if (class_exists($rule)) {
                                        $rules[] = $rule;
                                    }                           
                                }
                        }
                        return $rules;
                    });

                    foreach ($rules as $rule) {
                        $request = (new $rule())($request);
                    }
                //}
        
                // Continue with the request
                return $next($request);
            }
        }
        