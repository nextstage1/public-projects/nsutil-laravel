<?php

    use Illuminate\Support\Facades\Route;
        
    Route::group(['middleware' => [
        'Auth.check.token', 
        'Auth.check.permission', 
        'Company.check.belong.to'
        ]
    ], function () {
        Route::apiResource('company', \App\Modules\Company\Http\Controllers\CompanyController::class)->except(['store','destroy']);
Route::apiResource('permission', \App\Modules\Company\Http\Controllers\PermissionController::class)->except(['index', 'show', 'store', 'update', 'destroy']);
Route::apiResource('profile', \App\Modules\Company\Http\Controllers\ProfileController::class);
Route::apiResource('profile.profile-permission', \App\Modules\Company\Http\Controllers\ProfilePermissionController::class);
Route::apiResource('profile-permission', \App\Modules\Company\Http\Controllers\ProfilePermissionController::class);
Route::apiResource('user', \App\Modules\Company\Http\Controllers\UserController::class)->except(['store','destroy']);
Route::apiResource('profile.user-profile', \App\Modules\Company\Http\Controllers\UserProfileController::class);
Route::apiResource('user-profile', \App\Modules\Company\Http\Controllers\UserProfileController::class);
    });

    