<?php

    namespace App\Generated\Modules\Company\Providers;
    
    use Illuminate\Support\ServiceProvider;
    use Illuminate\Database\Eloquent\Factories\Factory as EloquentFactory;
    use Illuminate\Database\Seeder;
    use Illuminate\Support\Facades\Route;
    use Faker\Factory as FakerFactory;
    
    abstract class AbstractCompanyServiceProvider extends ServiceProvider
    {
    
        public function register()
        {
            // Não é necessário registrar a fábrica aqui.
        }
    
        public function boot()
        {
            // OK!
            Route::prefix('api')->group(function () {
                $this->loadRoutesFrom(base_path().'/app/Modules/Company/Routes/api.php');
            });

            $this->app['router']->middleware('Company.check.belong.to', \App\Modules\Acceptme\Http\Middlewares\CheckBelongTo::class);
    
            // OK!
            $this->loadMigrationsFrom([
                base_path() . '/app/Modules/Company/Database/Migrations',
                base_path() . '/app/Generated/Modules/Company/Database/Migrations'
            ]);
        
            // $this->loadViewsFrom(base_path().'/app/Modules/Company/Views', 'company');
    
            $this->publishes([
                base_path().'/app/Modules/Company/Config/company.php' => config_path('company.php'),
            ], 'company-config');
        }
    }
    