<?php

    namespace App\Generated\Modules\Company\Rules\ProfilePermission;
    
    use Illuminate\Http\Request;
    use NsUtilLaravel\Helpers\Helper;
    
    abstract class AbstractProfilePermissionVerifyBelongToRule
    {
        protected $request;

        public function __invoke(Request $request): Request
        {

            $this->request = $request;

            $this->inject();

            $this->rules();

            return $this->request;
        }

        protected function rules() : void {
            $merge = [];

            
        $checkJoin = [];

        $exists = \App\Modules\Company\Models\Profile::where('profiles.id', (int) $this->request->input('profile_id'))
        
        ->select('profiles.id')
        ->where('profiles.company_id', '=', $this->request->input('jwt_company_id'))
        ;

        foreach($checkJoin as $check) {
            if ((int) $this->request->input($check[1])>0) {
                $exists->where($check[0], '=', $this->request->input($check[1]));
            }
        }

        if (null !== $this->request->input('profile_id') && !$exists->exists())
        {
            throw new \Exception('The profile_id is not valid. (VLT-PS68-0870)', 422);
        };
         
        }

        protected function inject() : void {
            $merge = [];
            $merge['profile_id'] = (int) $this->request->route('profile');

            $toMerge = array_filter($merge, fn ($item) => $item > 0);
            if (count($toMerge) > 0) {
                $this->request->merge($toMerge);
            }

        }
    }
    