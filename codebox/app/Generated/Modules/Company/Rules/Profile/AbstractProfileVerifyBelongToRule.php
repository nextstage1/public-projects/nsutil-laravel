<?php

    namespace App\Generated\Modules\Company\Rules\Profile;
    
    use Illuminate\Http\Request;
    use NsUtilLaravel\Helpers\Helper;
    
    abstract class AbstractProfileVerifyBelongToRule
    {
        protected $request;

        public function __invoke(Request $request): Request
        {

            $this->request = $request;

            $this->inject();

            $this->rules();

            return $this->request;
        }

        protected function rules() : void {
            $merge = [];

            
        if (null !== $this->request->input('company_id') 
            && !\App\Modules\Company\Models\Company::where('id', (int) $this->request->input('company_id')
        )
            ->select('companies.id')
            ->where('companies.id', '=', $this->request->input('jwt_company_id'))
            ->exists()) {
            throw new \Exception('The company_id is not valid. (VLT-PSD9-999)', 422);
        };


        }

        protected function inject() : void {
            $merge = [];
            

            $toMerge = array_filter($merge, fn ($item) => $item > 0);
            if (count($toMerge) > 0) {
                $this->request->merge($toMerge);
            }

        }
    }
    