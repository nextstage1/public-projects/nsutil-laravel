<?php

    namespace App\Generated\Modules\Company\Rules\User;
    
    use Illuminate\Http\Request;
    use NsUtilLaravel\Helpers\Helper;
    
    abstract class AbstractUserVerifyBelongToRule
    {
        protected $request;

        public function __invoke(Request $request): Request
        {

            $this->request = $request;

            $this->inject();

            $this->rules();

            return $this->request;
        }

        protected function rules() : void {
            $merge = [];

            
        }

        protected function inject() : void {
            $merge = [];
            

            $toMerge = array_filter($merge, fn ($item) => $item > 0);
            if (count($toMerge) > 0) {
                $this->request->merge($toMerge);
            }

        }
    }
    