<?php

    namespace App\Generated\Modules\Company\Database\Seeders;
    
    use Illuminate\Database\Console\Seeds\WithoutModelEvents;
    use Illuminate\Database\Seeder;
    
    abstract class AbstractCompanySeeder extends Seeder
    {
        protected $qtdeToSeed = 10;

        /**
         * Run the database seeds.
         */
        public function run(): void
        {
            $models = [
                \App\Modules\Company\Models\Company::class,
\App\Modules\Company\Models\Permission::class,
\App\Modules\Company\Models\Profile::class,
\App\Modules\Company\Models\ProfilePermission::class,
\App\Modules\Company\Models\User::class,
\App\Modules\Company\Models\UserProfile::class
            ];
        
            foreach ($models as$model) {
                for ($i = 0; $i < $this->qtdeToSeed; $i++) {
                    $model::factory()->create();
                }
            }
        }
    }
       
    