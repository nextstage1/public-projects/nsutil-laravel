<?php

    namespace App\Generated\Modules\Company\Database\Factories;
    
    use Illuminate\Database\Eloquent\Factories\Factory;
    use App\Modules\Company\Models\ProfilePermission;
    
    /**
     * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Modules\Company\Models\ProfilePermission>
     */
    abstract class AbstractProfilePermissionFactory extends Factory
    {

        /**
         * The name of the factory's corresponding model.
         *
         * @var string
         */
        protected $model = ProfilePermission::class;


        /**
         * Define the model's default state.
         *
         * @return array<string, mixed>
         */
        public function definition(): array
        {
            $i=0;do {
 $i++;
$permission_id=\App\Modules\Company\Models\Permission::inRandomOrder()->first()->id;$profile_id=\App\Modules\Company\Models\Profile::inRandomOrder()->first()->id;
} while ($i<1000 && \App\Modules\Company\Models\ProfilePermission::where('permission_id', $permission_id)->where('profile_id', $profile_id)->exists());

            return [
                'permission_id' => $permission_id, 
'profile_id' => $profile_id
            ];
        }
    }
    