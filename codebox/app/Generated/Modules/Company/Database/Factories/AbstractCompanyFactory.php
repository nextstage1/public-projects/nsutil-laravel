<?php

    namespace App\Generated\Modules\Company\Database\Factories;
    
    use Illuminate\Database\Eloquent\Factories\Factory;
    use App\Modules\Company\Models\Company;
    
    /**
     * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Modules\Company\Models\Company>
     */
    abstract class AbstractCompanyFactory extends Factory
    {

        /**
         * The name of the factory's corresponding model.
         *
         * @var string
         */
        protected $model = Company::class;


        /**
         * Define the model's default state.
         *
         * @return array<string, mixed>
         */
        public function definition(): array
        {
            

            return [
                'name' => fake()->unique()->company(), 
'email' => fake()->unique()->safeEmail()
            ];
        }
    }
    