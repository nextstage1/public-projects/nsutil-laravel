<?php

    namespace App\Generated\Modules\Company\Database\Factories;
    
    use Illuminate\Database\Eloquent\Factories\Factory;
    use App\Modules\Company\Models\User;
    
    /**
     * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Modules\Company\Models\User>
     */
    abstract class AbstractUserFactory extends Factory
    {

        /**
         * The name of the factory's corresponding model.
         *
         * @var string
         */
        protected $model = User::class;


        /**
         * Define the model's default state.
         *
         * @return array<string, mixed>
         */
        public function definition(): array
        {
            $i=0;do {
 $i++;
$email = fake()->unique()->safeEmail();
} while ($i<1000 && \App\Modules\Company\Models\User::where('email', $email)->exists());

            return [
                'name' => fake()->name(), 
'email' => $email, 
'email_verified_at' => fake()->datetime()->format("Y-m-d H:i:s"), 
'password' => \Illuminate\Support\Facades\Hash::make(time())
            ];
        }
    }
    