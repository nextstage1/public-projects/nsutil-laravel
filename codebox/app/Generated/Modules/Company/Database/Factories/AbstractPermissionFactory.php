<?php

    namespace App\Generated\Modules\Company\Database\Factories;
    
    use Illuminate\Database\Eloquent\Factories\Factory;
    use App\Modules\Company\Models\Permission;
    
    /**
     * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Modules\Company\Models\Permission>
     */
    abstract class AbstractPermissionFactory extends Factory
    {

        /**
         * The name of the factory's corresponding model.
         *
         * @var string
         */
        protected $model = Permission::class;


        /**
         * Define the model's default state.
         *
         * @return array<string, mixed>
         */
        public function definition(): array
        {
            $i=0;do {
 $i++;
$route = fake()->text(50);$verb = fake()->text(50);
} while ($i<1000 && \App\Modules\Company\Models\Permission::where('route', $route)->where('verb', $verb)->exists());

            return [
                'group' => fake()->text(50), 
'route' => $route, 
'verb' => $verb
            ];
        }
    }
    