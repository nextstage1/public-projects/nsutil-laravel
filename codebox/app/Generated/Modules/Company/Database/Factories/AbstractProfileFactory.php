<?php

    namespace App\Generated\Modules\Company\Database\Factories;
    
    use Illuminate\Database\Eloquent\Factories\Factory;
    use App\Modules\Company\Models\Profile;
    
    /**
     * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Modules\Company\Models\Profile>
     */
    abstract class AbstractProfileFactory extends Factory
    {

        /**
         * The name of the factory's corresponding model.
         *
         * @var string
         */
        protected $model = Profile::class;


        /**
         * Define the model's default state.
         *
         * @return array<string, mixed>
         */
        public function definition(): array
        {
            $i=0;do {
 $i++;
$name = fake()->name();$company_id=\App\Modules\Company\Models\Company::inRandomOrder()->first()->id;
} while ($i<1000 && \App\Modules\Company\Models\Profile::where('name', $name)->where('company_id', $company_id)->exists());

            return [
                'name' => $name, 
'is_root' => fake()->boolean(), 
'company_id' => $company_id
            ];
        }
    }
    