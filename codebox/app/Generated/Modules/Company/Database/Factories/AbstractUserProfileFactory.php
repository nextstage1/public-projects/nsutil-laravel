<?php

    namespace App\Generated\Modules\Company\Database\Factories;
    
    use Illuminate\Database\Eloquent\Factories\Factory;
    use App\Modules\Company\Models\UserProfile;
    
    /**
     * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Modules\Company\Models\UserProfile>
     */
    abstract class AbstractUserProfileFactory extends Factory
    {

        /**
         * The name of the factory's corresponding model.
         *
         * @var string
         */
        protected $model = UserProfile::class;


        /**
         * Define the model's default state.
         *
         * @return array<string, mixed>
         */
        public function definition(): array
        {
            $i=0;do {
 $i++;
$user_id=\App\Modules\Company\Models\User::inRandomOrder()->first()->id;$profile_id=\App\Modules\Company\Models\Profile::inRandomOrder()->first()->id;
} while ($i<1000 && \App\Modules\Company\Models\UserProfile::where('user_id', $user_id)->where('profile_id', $profile_id)->exists());

            return [
                'user_id' => $user_id, 
'profile_id' => $profile_id
            ];
        }
    }
    