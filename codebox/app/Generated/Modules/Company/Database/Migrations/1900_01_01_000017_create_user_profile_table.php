<?php

    use Illuminate\Database\Migrations\Migration;
    use Illuminate\Database\Schema\Blueprint;
    use Illuminate\Support\Facades\Schema;
    
    return new class extends Migration
    {
        public function up()
        {
            Schema::create('user_profile', function (Blueprint $table) {
               $table->id('id')->comments('Id');
$table->timestamp('created_at')->nullable()->comments('Created at');
$table->timestamp('updated_at')->nullable()->comments('Updated at');
$table->timestamp('deleted_at')->nullable()->comments('Deleted at');
$table->foreignIdFor(\App\Modules\Company\Models\User::class, 'user_id')->constrained()->onUpdate('cascade')->onDelete('cascade');
$table->foreignIdFor(\App\Modules\Company\Models\Profile::class, 'profile_id')->constrained()->onUpdate('cascade')->onDelete('cascade');
$table->unique(['user_id','profile_id']);
            });
        }
    
        public function down()
        {
            Schema::dropIfExists('user_profile');
        }
    };