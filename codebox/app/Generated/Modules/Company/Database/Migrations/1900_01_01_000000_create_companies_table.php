<?php

    use Illuminate\Database\Migrations\Migration;
    use Illuminate\Database\Schema\Blueprint;
    use Illuminate\Support\Facades\Schema;
    
    return new class extends Migration
    {
        public function up()
        {
            Schema::create('companies', function (Blueprint $table) {
               $table->id('id')->comments('Id');
$table->timestamp('created_at')->nullable()->comments('Created at');
$table->timestamp('updated_at')->nullable()->comments('Updated at');
$table->timestamp('deleted_at')->nullable()->comments('Deleted at');
$table->string('name', '255')->comments('Name');
$table->string('email', '255')->comments('Email');
            });
        }
    
        public function down()
        {
            Schema::dropIfExists('companies');
        }
    };