<?php

    use Illuminate\Database\Migrations\Migration;
    use Illuminate\Database\Schema\Blueprint;
    use Illuminate\Support\Facades\Schema;
    
    return new class extends Migration
    {
        public function up()
        {
            Schema::create('profiles', function (Blueprint $table) {
               $table->id('id')->comments('Id');
$table->timestamp('created_at')->nullable()->comments('Created at');
$table->timestamp('updated_at')->nullable()->comments('Updated at');
$table->timestamp('deleted_at')->nullable()->comments('Deleted at');
$table->string('name', '100')->comments('Name');
$table->boolean('is_root')->comments('Is root');
$table->foreignIdFor(\App\Modules\Company\Models\Company::class, 'company_id')->constrained()->onUpdate('cascade')->onDelete('cascade');
$table->unique(['name','company_id']);
            });
        }
    
        public function down()
        {
            Schema::dropIfExists('profiles');
        }
    };