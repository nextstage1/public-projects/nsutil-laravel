<?php

    use Illuminate\Database\Migrations\Migration;
    use Illuminate\Database\Schema\Blueprint;
    use Illuminate\Support\Facades\Schema;
    
    return new class extends Migration
    {
        public function up()
        {
            Schema::create('profile_permissions', function (Blueprint $table) {
               $table->id('id')->comments('Id');
$table->foreignIdFor(\App\Modules\Company\Models\Permission::class, 'permission_id')->constrained()->onUpdate('cascade')->onDelete('cascade');
$table->foreignIdFor(\App\Modules\Company\Models\Profile::class, 'profile_id')->constrained()->onUpdate('cascade')->onDelete('cascade');
$table->timestamp('created_at')->nullable()->comments('Created at');
$table->timestamp('updated_at')->nullable()->comments('Updated at');
$table->unique(['permission_id','profile_id']);
            });
        }
    
        public function down()
        {
            Schema::dropIfExists('profile_permissions');
        }
    };