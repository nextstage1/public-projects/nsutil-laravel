<?php

    namespace App\Generated\Modules\Company\Models;
    
        
                        use Illuminate\Database\Eloquent\Factories\HasFactory;
                        use Illuminate\Database\Eloquent\Model;
                        use Illuminate\Database\Eloquent\SoftDeletes;
                        use Illuminate\Database\Eloquent\Prunable;
                        

    use App\Modules\Company\Database\Factories\ProfileFactory;
    
    abstract class AbstractProfile extends Model
    {
        use HasFactory, SoftDeletes, Prunable;

        protected $table = 'profiles';
        protected $fillable = ['name','is_root','company_id'];

        public function profilePermissions()
        {
            return $this->hasMany(\App\Modules\Company\Models\ProfilePermission::class);
        }
        
public function userProfile()
        {
            return $this->hasMany(\App\Modules\Company\Models\UserProfile::class);
        }
        
public function company()
        {
            return $this->belongsTo(\App\Modules\Company\Models\Company::class);
        }
        

        
                    public function prunable()
                    {
                        return static::where('deleted_at', '<=', now()->subMonth());
                    }
                    

        protected static function newFactory(): ProfileFactory
        {
            return new ProfileFactory();
        }
    }
    