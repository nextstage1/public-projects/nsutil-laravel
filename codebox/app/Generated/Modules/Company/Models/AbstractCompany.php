<?php

    namespace App\Generated\Modules\Company\Models;
    
        
                        use Illuminate\Database\Eloquent\Factories\HasFactory;
                        use Illuminate\Database\Eloquent\Model;
                        use Illuminate\Database\Eloquent\SoftDeletes;
                        use Illuminate\Database\Eloquent\Prunable;
                        

    use App\Modules\Company\Database\Factories\CompanyFactory;
    
    abstract class AbstractCompany extends Model
    {
        use HasFactory, SoftDeletes, Prunable;

        protected $table = 'companies';
        protected $fillable = ['name','email'];

        public function documentTypes()
        {
            return $this->hasMany(\App\Modules\Acceptme\Models\DocumentType::class);
        }
        
public function products()
        {
            return $this->hasMany(\App\Modules\Acceptme\Models\Product::class);
        }
        
public function profiles()
        {
            return $this->hasMany(\App\Modules\Company\Models\Profile::class);
        }
        

        
                    public function prunable()
                    {
                        return static::where('deleted_at', '<=', now()->subMonth());
                    }
                    

        protected static function newFactory(): CompanyFactory
        {
            return new CompanyFactory();
        }
    }
    