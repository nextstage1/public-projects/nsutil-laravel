<?php

    namespace App\Generated\Modules\Company\Models;
    
    use Illuminate\Database\Eloquent\Factories\HasFactory;
    use Illuminate\Foundation\Auth\User as Authenticatable;
    use Illuminate\Notifications\Notifiable;
    use Illuminate\Database\Eloquent\SoftDeletes;
    use Illuminate\Database\Eloquent\Prunable;
    use Tymon\JWTAuth\Contracts\JWTSubject;
    use App\Modules\Company\Database\Factories\UserFactory;
    
    class AbstractUser extends Authenticatable implements JWTSubject
    {
        use HasFactory, Notifiable,  SoftDeletes, Prunable;

        protected $primaryKey = 'id';
    
        /**
         * The attributes that are mass assignable.
         *
         * @var array<int, string>
         */
        protected $fillable = [
            'name',
            'email',
            'password',
        ];
    
        /**
         * The attributes that should be hidden for serialization.
         *
         * @var array<int, string>
         */
        protected $hidden = [
            'password',
            'remember_token',
        ];
    
        /**
         * The attributes that should be cast.
         *
         * @var array<string, string>
         */
        protected $casts = [
            'email_verified_at' => 'datetime',
        ];
    
    
        public function getJWTIdentifier()
        {
            return $this->getKey();
        }
    
        public function getJWTCustomClaims()
        {
            return [];
        }
    
        public function prunable()
        {
            return static::where('deleted_at', '<=', now()->subMonth());
        }

        protected static function newFactory(): UserFactory
        {
            return new UserFactory();
        }
    }
    