<?php

    namespace App\Generated\Modules\Company\Models;
    
        
    use Illuminate\Database\Eloquent\Factories\HasFactory;
    use Illuminate\Database\Eloquent\Model;
    

    use App\Modules\Company\Database\Factories\ProfilePermissionFactory;
    
    abstract class AbstractProfilePermission extends Model
    {
        use HasFactory;

        protected $table = 'profile_permissions';
        protected $fillable = ['permission_id','profile_id'];

        public function permission()
        {
            return $this->belongsTo(\App\Modules\Company\Models\Permission::class);
        }
        
public function profile()
        {
            return $this->belongsTo(\App\Modules\Company\Models\Profile::class);
        }
        

        

        protected static function newFactory(): ProfilePermissionFactory
        {
            return new ProfilePermissionFactory();
        }
    }
    