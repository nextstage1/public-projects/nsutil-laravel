<?php

    namespace App\Generated\Modules\Company\Models;
    
        
                        use Illuminate\Database\Eloquent\Factories\HasFactory;
                        use Illuminate\Database\Eloquent\Model;
                        use Illuminate\Database\Eloquent\SoftDeletes;
                        use Illuminate\Database\Eloquent\Prunable;
                        

    use App\Modules\Company\Database\Factories\UserProfileFactory;
    
    abstract class AbstractUserProfile extends Model
    {
        use HasFactory, SoftDeletes, Prunable;

        protected $table = 'user_profile';
        protected $fillable = ['user_id','profile_id'];

        public function user()
        {
            return $this->belongsTo(\App\Modules\Company\Models\User::class);
        }
        
public function profile()
        {
            return $this->belongsTo(\App\Modules\Company\Models\Profile::class);
        }
        

        
                    public function prunable()
                    {
                        return static::where('deleted_at', '<=', now()->subMonth());
                    }
                    

        protected static function newFactory(): UserProfileFactory
        {
            return new UserProfileFactory();
        }
    }
    