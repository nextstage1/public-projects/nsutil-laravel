<?php

    namespace App\Generated\Modules\Company\Models;
    
        
    use Illuminate\Database\Eloquent\Factories\HasFactory;
    use Illuminate\Database\Eloquent\Model;
    

    use App\Modules\Company\Database\Factories\PermissionFactory;
    
    abstract class AbstractPermission extends Model
    {
        use HasFactory;

        protected $table = 'permissions';
        protected $fillable = ['group','route','verb'];

        public function profilePermissions()
        {
            return $this->hasMany(\App\Modules\Company\Models\ProfilePermission::class);
        }
        

        

        protected static function newFactory(): PermissionFactory
        {
            return new PermissionFactory();
        }
    }
    