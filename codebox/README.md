# Accept.Me by 5Labs

Accept.Me by 5Labs é um aplicativo desenvolvido em Laravel 10 e PostgreSQL, projetado para gerenciar e monitorar aceites e rejeições de termos de uso, políticas de privacidade e outros acordos legais. Este aplicativo é hospedado em uma VM escalável do Google Cloud e gera relatórios de Business Intelligence (BI) para fornecer insights valiosos sobre a quantidade de aceites e rejeições dos termos.

## Características

-   **Gestão de termos:** Edite online as versões dos termos ou envie um PDF para exibição.
-   **Relatórios de BI:** Visualize relatórios detalhados em formatos de BI sobre aceites e rejeições dos termos.
-   **Integração fácil:** Os clientes podem inserir um script em suas páginas da web para exibir um modal com os termos e condições.
-   **Controle de acesso:** Configure se, após a rejeição dos termos, os usuários continuarão navegando ou terão seu acesso bloqueado.

## Requisitos para desenvolvimento

-   Docker (com WSL2 para windows)

## Iniciando

1. Clone este repositório:

`git clone https://github.com/nextstage1/laravel10-boilerplate.git`

2. Configure o arquivo .env com as informações da sua instância do PostgreSQL e da Google Cloud VM.

3. Inicie o servidor de desenvolvimento:

```
sudo docker-compose up -d --build
sudo docker ps
sudo docker exec -it NOME_CONTAINER bash
```

Importante: trabalhe sempre dentro do shell do container e não do WSL para manter a conexão com as versões do PHP selecionado na ENV

4. Rode as migrações e seeds do banco de dados e gere o JWT token

```

### Builder
Caso precise de automação para gerar algum arquivo extra:
1. Configure o arquivo .build.config.json na raiz
2. Execute no terminal: php artisan build

Este comando lê o banco de dados e gera os arquivos. Portanto, crie e rode as migrations antes.



Agora, seu aplicativo Accept.Me by 5Labs está pronto para uso. Basta seguir as instruções para configurar e personalizar as opções de termos e políticas de acordo com suas necessidades.




APIKEY =====
google =====
apple ======
```
