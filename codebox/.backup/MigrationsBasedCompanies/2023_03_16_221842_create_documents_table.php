<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('documents', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->softDeletes();

            $table->string('title', 255)->comment('Title of document');
            $table->timestamp('published_at')->nullable()->comment('Publication date');
            $table->foreignId('product_id')->constrained('products')->onDelete('restrict')->onUpdate('cascade')->comment('Product');
            $table->foreignId('document_type_id')->constrained('document_types')->onDelete('restrict')->onUpdate('cascade')->comment('Document type');

            $table->unique('title', 'product_id');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('documents');
    }
};
