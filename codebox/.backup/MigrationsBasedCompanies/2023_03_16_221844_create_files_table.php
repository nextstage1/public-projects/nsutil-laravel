<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('files', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->softDeletes();
            $table->string('filename', 150)->comment('Path of filename')->unique();
            $table->string('name', 150)->comment('Filename')->nullable();
            $table->string('mime', 50)->comment('Mime type');
            $table->foreignId('version_id')->constrained('versions')->onDelete('cascade')->onUpdate('cascade')->comment('Version');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('files');
    }
};
