<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->softDeletes();
            $table->string('name', 200)->comment('Product name');
            $table->string('external_id')->nullable()->comment('External ID or reference for product');
            $table->foreignId('company_id')->constrained('companies')->onDelete('cascade')->onUpdate('cascade')->comment('Company');

            $table->unique(['name', 'external_id',  'company_id']);
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('products');
    }
};
