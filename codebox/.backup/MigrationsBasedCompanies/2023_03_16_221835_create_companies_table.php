<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    public function up(): void
    {
        Schema::create('companies', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->softDeletes();
            $table->string('name')->unique()->comment('Company name');
            $table->string('email')->unique()->comment('Company e-mail');
        });
    }

    public function down()
    {
        Schema::dropIfExists('companies');
    }
};
