<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('registers', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->softDeletes();
            $table->ipAddress('ip');
            $table->string('username')->comment('REF to username');
            $table->string('external_id', 128)->comment('External ID to integration of client');
            $table->jsonb('fingerprint')->comment('Fingerprint of APP or Browser');
            $table->foreignId('version_id')->constrained('versions')->onDelete('restrict')->onUpdate('cascade')->comment('Version');
            $table->foreignId('register_type_id')->constrained('register_types')->onDelete('restrict')->onUpdate('cascade')->comment('Type of register');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('registers');
    }
};
