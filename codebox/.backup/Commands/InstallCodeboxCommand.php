<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;
use NsUtilLaravel\Helpers\Helper;
use Symfony\Component\Console\Input\InputOption;

class InstallCodeboxCommand extends Command
{

    protected function configure()
    {
        $this->setName('app:codebox');
    }

    public function handle(): void
    {
        Helper::commandPrintHeader('Install Initial Files');

        Helper::shellExec('composer install --quiet');

        shell_exec("php vendor/nextstage-brasil/nsutil-laravel/codebox/install.php");
    }
}
