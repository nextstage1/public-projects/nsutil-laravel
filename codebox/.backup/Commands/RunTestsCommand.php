<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;
use NsUtilLaravel\Helpers\Helper;
use Symfony\Component\Console\Input\InputOption;

class RunTestsCommand extends Command
{

    protected function configure()
    {
        $this->setName('app:test');
    }

    public function handle(): void
    {
        Helper::commandPrintHeader('Run Tests Pest');

        Helper::shellExec('composer install --quiet');

        $command = 'cd ' . Helper::getPathApp()
            . ' && '
            . "./vendor/bin/pest --testdox --coverage-html ./coverage tests/Feature";

        Helper::shellExec($command);
    }
}
