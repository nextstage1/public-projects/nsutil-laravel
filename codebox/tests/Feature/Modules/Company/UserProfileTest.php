<?php

    uses(Illuminate\Foundation\Testing\RefreshDatabase::class);
    
    use Illuminate\Support\Str;
    use Tymon\JWTAuth\Facades\JWTAuth;
    use App\Modules\Company\Models\UserProfile as ModelTest;
    use Illuminate\Support\Facades\Hash;
    
    beforeEach(function () {
        $this->company = \App\Modules\Company\Models\Company::factory()->create([
            'name' => 'Testes Unitarios'
        ]);
    
        $profile = \App\Modules\Company\Models\Profile::factory()->create([
            'name' => 'Testes Unitarios',
            'company_id' => $this->company->id, 
            'is_root' => true
        ]);
    
        $this->user = \App\Modules\Company\Models\User::factory()->create([
            'name' => 'Tester PhpPest',
            'email' => 'tester-pest@local.com', 
            'password' => Hash::make('1234567890')
        ]);
    
        $this->company_user = \App\Modules\Company\Models\UserProfile::factory()->create([
            'user_id' => $this->user->id,
            'profile_id' => $profile->id
        ]);

        // Login to get token
        $modelLogin = [
            'email' => $this->user->email,
            'password' => '1234567890',
            'company_id' => $this->company->id
        ];
        $response = $this->postJson('/api/auth/login', $modelLogin);
        $this->token = $response->json('data.access_token');

        \App\Modules\Company\Models\User::factory()->create();\App\Modules\Company\Models\Profile::factory()->create();
    
    });


    
        test('can create a userProfile', function ()  {
            $data = ['company_id' => $this->company->id];
            $modelData = ModelTest::factory()->make($data)->toArray();        
            $response = $this->withHeaders(['Authorization' => 'Bearer ' . $this->token])->postJson('/api/user-profile', $modelData);

            // See errors
            $error = $response->json('meta.error');
            if (false !== $error) {
                echo "--- Error: ";
                echo json_encode($response->json('meta.error'));
                echo "\n";
            }

            $response->assertStatus(201);
            $response->assertJsonStructure([
                'data' => ['id']
            ]);
        });

        test('can read a userProfile', function ()  {
            $model = ModelTest::factory()->create();
            $response = $this->withHeaders(['Authorization' => 'Bearer ' . $this->token])->getJson('/api/user-profile/' . $model->id);
            
            // See errors
            $error = $response->json('meta.error');
            if (false !== $error) {
                echo "--- Error: ";
                echo json_encode($response->json('meta.error'));
                echo "\n";
            }


            $response->assertStatus(200);
            $response->assertJsonStructure([
                'data' => ['id']
            ]);
        });
        

        test('can list userProfile', function ()  {
            ModelTest::factory()->count(1)->create();
            $response = $this->withHeaders(['Authorization' => 'Bearer ' . $this->token])->getJson('/api/user-profile');
            
            // See errors
            $error = $response->json('meta.error');
            if (false !== $error) {
                echo "--- Error: ";
                echo json_encode($response->json('meta.error'));
                echo "\n";
            }


            $response->assertStatus(200);
            $response->assertJsonStructure([
                'data' => ['*' => ['id']]
            ]);
        });
        

        test('can update a userProfile', function ()  {
            $model = ModelTest::factory()->create();
            $updatedData = ModelTest::factory()->make(['company_id' => $this->company->id])->toArray();
            $response = $this->withHeaders(['Authorization' => 'Bearer ' . $this->token])->putJson('/api/user-profile/' . $model->id, $updatedData);

            // See errors
            $error = $response->json('meta.error');
            if (false !== $error) {
                echo "--- Error: ";
                echo json_encode($response->json('meta.error'));
                echo "\n";
            }

            $response->assertStatus(200);
            $response->assertJsonStructure([
                'data' => ['id']
            ]);
        });
        

        test('can delete a userProfile', function ()  {
            $model = ModelTest::factory()->create();
            $response = $this->withHeaders(['Authorization' => 'Bearer ' . $this->token])->deleteJson('/api/user-profile/' . $model->id);
            
            // See errors
            $error = $response->json('meta.error');
            if (false !== $error) {
                echo "--- Error: ";
                echo json_encode($response->json('meta.error'));
                echo "\n";
            }


            $response->assertStatus(204);
        });
        
    