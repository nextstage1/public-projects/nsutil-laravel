<?php

uses(Illuminate\Foundation\Testing\RefreshDatabase::class);

use Illuminate\Support\Str;
use Tymon\JWTAuth\Facades\JWTAuth;
use App\Modules\Company\Models\Company as ModelTest;
use Illuminate\Support\Facades\Hash;

beforeEach(function () {
    $this->company = \App\Modules\Company\Models\Company::factory()->create([
        'name' => 'Testes Unitarios'
    ]);

    $profile = \App\Modules\Company\Models\Profile::factory()->create([
        'name' => 'Testes Unitarios',
        'company_id' => $this->company->id,
        'is_root' => true
    ]);

    $this->user = \App\Modules\Company\Models\User::factory()->create([
        'name' => 'Tester PhpPest',
        'email' => 'tester-pest@local.com',
        'password' => Hash::make('1234567890')
    ]);

    $this->company_user = \App\Modules\Company\Models\UserProfile::factory()->create([
        'user_id' => $this->user->id,
        'profile_id' => $profile->id
    ]);

    // Login to get token
    $modelLogin = [
        'email' => $this->user->email,
        'password' => '1234567890',
        'company_id' => $this->company->id
    ];
    $response = $this->postJson('/api/auth/login', $modelLogin);
    $this->token = $response->json('data.access_token');
});



test('cannot create a company', function () {
    $data = ['company_id' => $this->company->id];
    $modelData = ModelTest::factory()->make($data)->toArray();
    $response = $this->withHeaders(['Authorization' => 'Bearer ' . $this->token])->postJson('/api/company', $modelData);
    $response->assertStatus(405);
});

test('can read my company', function () {
    $response = $this->withHeaders(['Authorization' => 'Bearer ' . $this->token])->getJson('/api/company/' . $this->company->id);
    $response->assertStatus(200);
});


test('can list my company', function () {
    ModelTest::factory()->count(1)->create();
    $response = $this->withHeaders(['Authorization' => 'Bearer ' . $this->token])->getJson('/api/company');
    $response->assertStatus(200);
    $response->assertJsonStructure([
        'data' => ['*' => ['id', 'name', 'email']]
    ]);
});


test('can update my company', function () {
    $updatedData = ModelTest::factory()->make(['name' => 'new name updated'])->toArray();
    $response = $this->withHeaders(['Authorization' => 'Bearer ' . $this->token])->putJson('/api/company/' . $this->company->id, $updatedData);
    // See errors
    $error = $response->json('meta.error');
    if (false !== $error) {
        echo "--- Error: ";
        echo json_encode($response->json('meta.error'));
        echo "\n";
    }
    $response->assertStatus(200);
    $response->assertJsonStructure([
        'data' => ['id', 'name', 'email']
    ]);
});


test('cannot delete my and any company', function () {
    $model = ModelTest::factory()->create();
    $response = $this->withHeaders(['Authorization' => 'Bearer ' . $this->token])->deleteJson('/api/company/' . $model->id);
    $response->assertStatus(405);
});
