<?php

uses(Illuminate\Foundation\Testing\RefreshDatabase::class);

use Illuminate\Support\Facades\Hash;


beforeEach(function () {
    $this->company = \App\Modules\Company\Models\Company::factory()->create([
        'name' => 'Testes Unitarios'
    ]);

    $profile = \App\Modules\Company\Models\Profile::factory()->create([
        'name' => 'Testes Unitarios',
        'company_id' => $this->company->id,
        'is_root' => true
    ]);

    $this->user = \App\Modules\Company\Models\User::factory()->create([
        'name' => 'Tester PhpPest',
        'email' => 'tester-pest@local.com',
        'password' => Hash::make('1234567890')
    ]);

    $this->company_user = \App\Modules\Company\Models\UserProfile::factory()->create([
        'user_id' => $this->user->id,
        'profile_id' => $profile->id
    ]);

    // Login to get token
    $modelLogin = [
        'email' => $this->user->email,
        'password' => '1234567890',
        'company_id' => $this->company->id
    ];
    $response = $this->postJson('/api/auth/login', $modelLogin);
    $this->token = $response->json('data.access_token');
});




test('can login', function () {
    // Login to get token
    $modelLogin = [
        'email' => $this->user->email,
        'password' => '1234567890',
        'company_id' => $this->company->id
    ];
    $response = $this->postJson('/api/auth/login', $modelLogin);

    $response->assertStatus(200);
    $response->assertJsonStructure([
        'data' => [
            'access_token',
            'token_type',
            'expires_in',
            'user' => [
                'id',
                'name',
                'email',
            ],
            'company' => [
                'id',
                'name',
                'email',
            ],
        ]
    ]);
});

test('cannot login with invalid credentials', function () {
    // Login to get token
    $modelLogin = [
        'email' => $this->user->email,
        'password' => '1234567890ERROR',
        'company_id' => $this->company->id
    ];
    $response = $this->postJson('/api/auth/login', $modelLogin);

    $response->assertStatus(401);
    $response->assertJsonStructure([
        'meta' => ['error'],
    ]);
});

test('can logout', function () {
    $response = $this->withHeaders(['Authorization' => 'Bearer ' . $this->token])->postJson('/api/auth/logout', []);
    $response->assertStatus(200);
    $response->assertJsonStructure([
        'meta' => ['message'],
    ]);
    $response->assertJsonFragment([
        'data' => [
            'message' => 'Logged out successfully'
        ]
    ]);
});

test('can not navigate after logout', function () {
    $response = $this->withHeaders(['Authorization' => 'Bearer ' . $this->token])->postJson('/api/auth/logout', []);
    $response = $this->withHeaders(['Authorization' => 'Bearer ' . $this->token])->getJson('/api/user/' . $this->user->id);
    $response->assertStatus(401);
    $response->assertJsonStructure([
        'meta' => ['message'],
    ]);
});
