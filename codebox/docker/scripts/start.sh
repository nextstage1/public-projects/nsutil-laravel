#!/bin/bash

# Define a variável __DIR__ com o diretório atual do script
__DIR__="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
cd "$__DIR__/../"

# criar env default caso não exista
if ! [ -f "$__DIR__/..env" ]; then
    cp cp .env.example .env
fi

# preparar aplicação
source "$__DIR__/_init.sh"

createPersistPath
dockerPrune
dockerPull
dockerUp
waitForContainerHealth "app_db app_api"
printContainers
