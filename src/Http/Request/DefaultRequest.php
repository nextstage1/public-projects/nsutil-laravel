<?php

namespace NsUtilLaravel\Http\Request;


class DefaultRequest extends \Illuminate\Foundation\Http\FormRequest
{

    protected function failedValidation(\Illuminate\Contracts\Validation\Validator $validator)
    {
        $failed = array_map(fn ($item) => $item[0], $validator->getMessageBag()->toArray());
        throw new \Illuminate\Http\Exceptions\HttpResponseException(
            response()->json(['error' => $failed], 422)
        );
    }

    public function rulesDefault(array $rules = [])
    {
        // Adicionar sometimes aos itens
        if ($this->method() === 'PUT') {
            $rules = array_map(fn ($item) => is_array($item) ? $item : "sometimes|$item", $rules);
        }

        return ($this->method === 'POST' || $this->method === 'PUT')
            ? $rules
            : [];
    }

    public function authorize()
    {
        return false;
    }
}
