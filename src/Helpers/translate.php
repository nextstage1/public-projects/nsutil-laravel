<?php

use Illuminate\Support\Facades\Log;
use NsUtilLaravel\Helpers\Helper;

if (!function_exists('__tr')) {
    function __tr($key, $replace = [], $locale = null)
    {
        if (!is_string($key)) {
            return $key;
        }
        $currentLocale = app()->getLocale();
        $file = Helper::getPathApp() . "/lang/{$currentLocale}.json";
        $data = file_exists($file) ? json_decode(file_get_contents($file), true) ?? [] : [];
        $translate = $data[$key] ?? null;
        if (null !== $translate) {
            return $translate;
        }

        if (!file_exists($file) || is_writable($file)) {
            $data[$key] = $key;
            ksort($data);
            Helper::saveFile($file, json_encode($data, JSON_UNESCAPED_SLASHES), 'SOBREPOR');
        } else {
            Log::debug('Translate file is not writable: ' . $file);
        }

        return $key;
    }
}
