<?php

namespace NsUtilLaravel\Helpers;

use Illuminate\Support\Facades\Log;

class Helper
{

    public static $DBERRORS = [
        '01000' => 'Warning: General warning',
        '01004' => 'Warning: String data, right-truncated',
        '01006' => 'Warning: Privilege not revoked',
        '01007' => 'Warning: Privilege not granted',
        '01S00' => 'Invalid connection string attribute',
        '07001' => 'Warning: Wrong number of parameters',
        '07002' => 'Warning: Count field incorrect',
        '07005' => 'Warning: Prepared statement not executed',
        '07006' => 'Warning: Restricted data type attribute violation',
        '07009' => 'Warning: Invalid descriptor index',
        '08001' => 'Client unable to establish connection',
        '08002' => 'Connection name in use',
        '08003' => 'Connection does not exist',
        '08004' => 'Server rejected the connection',
        '08006' => 'Connection failure',
        '08S01' => 'Communication link failure',
        '21S01' => 'Insert value list does not match column list',
        '22001' => 'String data right truncation',
        '22002' => 'Indicator variable required but not supplied',
        '22003' => 'Numeric value out of range',
        '22007' => 'Invalid datetime format',
        '22008' => 'Datetime field overflow',
        '22012' => 'Division by zero',
        '22015' => 'Interval field overflow',
        '22018' => 'Invalid character value for cast specification',
        '22025' => 'Invalid escape character',
        '23000' => 'Integrity constraint violation',
        '24000' => 'Invalid cursor state',
        '28000' => 'Invalid authorization specification',
        '34000' => 'Invalid cursor name',
        '3D000' => 'Invalid catalog name',
        '40001' => 'Serialization failure',
        '40003' => 'Statement completion unknown',
        '42000' => 'Syntax error or access violation',
        '42S01' => 'Base table or view already exists',
        '42S02' => 'Base table or view not found',
        '42S11' => 'Index already exists',
        '42S12' => 'Index not found',
        '42S21' => 'Column already exists',
        '42S22' => 'Column not found',
        'HY000' => 'General error',
        'HY001' => 'Memory allocation error',
        'HY004' => 'Invalid SQL data type',
        'HY008' => 'Operation canceled',
        'HY009' => 'Invalid use of null pointer',
        'HY010' => 'Function sequence error',
        'HY011' => 'Attribute cannot be set now',
        'HYT00' => 'Timeout expired',
        'IM001' => 'Driver does not support this function',
        'IM017' => 'Polling is disabled',
        '23502' => 'Null value not allowed - check constraint violation',
        '23503' => 'Foreign key violation',
        '23505' => 'Unique constraint violation',
        '23514' => 'Check constraint violation',
        '24000' => 'Invalid cursor state',
        '40002' => 'Transaction rollback',
        '42000' => 'Syntax error or access violation',
        '42S02' => 'Table not found',
        '42S22' => 'Column not found',
        'HY000' => 'General error',
        '25P02' => 'Transaction aborted due to conflicts with concurrent transaction',
        '42703' => 'Column not found'
    ];

    public const IF_NOT_UPDATE = 'IF_NOT_UPDATED';
    public const OVERWRITE = 'SOBREPOR';

    /**
     * Retorna a string no formato camelCase
     * @param string|array $string
     * @param array $prefixo
     * @return string|array
     */
    public static function name2CamelCase($string, $prefixo = false)
    {
        $prefixo = array('mem_', 'sis_', 'anz_', 'aux_', 'app_');
        if (is_array($string)) {
            foreach ($string as $key => $value) {
                $out[self::name2CamelCase($key)] = $value;
            }
            return $out;
        }
        if (is_array($prefixo)) {
            foreach ($prefixo as $val) {
                $string = str_replace($val, "", $string);
            }
        }

        $string = str_replace('_', ' ', $string);
        $string = str_replace('-', ' ', $string);
        //        $out = str_replace(' ', '', ucwords($string));
        $out = lcfirst(str_replace(' ', '', ucwords($string)));
        return $out;
    }

    /**
     * Revert a string camelCase para camel_case
     * @param string $string
     * @return string
     */
    public static function reverteName2CamelCase($string): string
    {
        $out = '';
        for ($i = 0; $i < strlen((string) $string); $i++) {
            if ($string[$i] === mb_strtoupper((string)$string[$i]) && $string[$i] !== '.') {
                $out .= (($i > 0) ? '_' : '');
                $string[$i] = mb_strtolower($string[$i]);
            }
            $out .= $string[$i];
        }
        return (string) $out;
    }

    public static function name2KebabCase($string, $prefixo = false)
    {
        if (stripos($string, '_') === false) {
            $string = self::reverteName2CamelCase($string);
        }

        return str_replace('_', '-', $string);
    }

    public static function compareString($str1, $str2, $case = false)
    {
        if (!$case) {
            return (mb_strtoupper((string)$str1) === mb_strtoupper((string)$str2));
        } else {
            return ($str1 === $str2);
        }
    }

    /**
     * Ira buscar o path da aplicação, antes da path /vendor
     */
    public static function getPathApp()
    {
        $parts = explode('/vendor/', __DIR__);
        return $parts[0];
    }

    public static function mkdir($path, $perm = 0777): void
    {
        if (!is_dir($path) && !is_file($path)) {
            @mkdir($path, $perm, true);
        }
    }

    /**
     * Cria a arvore de diretorios
     * @param string $filename
     * @return object
     */
    public static function createTreeDir($filename)
    {
        $path = str_replace('/', DIRECTORY_SEPARATOR, (string) $filename);
        $parts = explode(DIRECTORY_SEPARATOR, $path);
        $file = array_pop($parts);
        $dir = implode(DIRECTORY_SEPARATOR, $parts);
        self::mkdir($dir, 0777);
        // @mkdir($dir, 0777, true);
        return (object) ['path' => $dir, 'name' => $file];
    }

    /**
     * Salva um arquivo no disco
     *
     * @param string $filename
     * @param string $template
     * @param string $mode
     * @return void
     */
    public static function saveFile(string $filename, string $template = '', string $mode = ''): bool
    {
        if (file_exists($filename)) {

            if ($mode === 'IF_NOT_UPDATED') {
                $criacao = filectime($filename);
                $atualizacao = filemtime($filename);
                if ($atualizacao <= $criacao) {
                    // Sem alteração, sobrepor
                    return self::saveFile($filename, $template, 'SOBREPOR');
                }
                Log::debug("Atualização posterior a criação: $atualizacao > $criacao");
            }

            $filename = dirname($filename)
                . DIRECTORY_SEPARATOR
                . ($mode !== 'SOBREPOR' ? '__NEW__' : '')
                . basename($filename);

            $mode !== 'SOBREPOR' ? Log::debug($filename) : null;
        } else {
            self::createTreeDir($filename);
        }

        file_put_contents($filename, $template);
        return file_exists($filename);
    }

    public static  function singularize($word)
    {
        $pluralEndings = [
            '/(alias|address)es$/i' => '\1',
            '/([^aeiouy])ies$/i' => '\1y',
            '/(s)$/i' => '',
            '/(ss)$/i' => 'ss',
            '/(n)ews$/i' => '\1ews',
            '/(r)ice$/i' => '\1ice',
            '/(children)$/i' => 'child',
            '/(m)en$/i' => '\1an',
            '/(t)eeth$/i' => '\1ooth',
            '/(f)eet$/i' => '\1oot',
            '/(g)eese$/i' => '\1oose',
            '/(m)ice$/i' => '\1ouse',
            '/(x|ch|ss|sh)es$/i' => '\1',
            '/(m)ovies$/i' => '\1ovie',
            '/(s)eries$/i' => '\1eries',
            '/([^aeiouy]|qu)ies$/i' => '\1y',
            '/([lr])ves$/i' => '\1f',
            '/(tive)s$/i' => '\1',
            '/(hive)s$/i' => '\1',
            '/(pri)ces$/i' => '\1ce',
            '/(b)uses$/i' => '\1us',
            '/(shoe)s$/i' => '\1',
            '/(o)es$/i' => '\1',
            '/(ax|test)es$/i' => '\1is',
            '/(octop|vir)i$/i' => '\1us',
            '/(alias|status)es$/i' => '\1',
            '/s$/i' => '',
        ];

        foreach ($pluralEndings as $pattern => $replacement) {
            if (preg_match($pattern, $word)) {
                return preg_replace($pattern, $replacement, $word);
            }
        }

        return $word;
    }

    public static function pluralize($word)
    {
        $singularEndings = [
            '/(quiz)$/i' => '\1zes',
            '/(matr|vert|ind)ix|ex$/i' => '\1ices',
            '/(x|ch|ss|sh)$/i' => '\1es',
            '/(r|t|h|s|z)$/i' => '\1es',
            '/([^aeiouy]|qu)ies$/i' => '\1y',
            '/(m)ovies$/i' => '\1ovie',
            '/(s)eries$/i' => '\1eries',
            '/(n)ews$/i' => '\1ews',
            '/(child)$/i' => '\1ren',
            '/(bus)$/i' => '\1es',
            '/(woman)$/i' => '\1women',
            '/(man)$/i' => '\1men',
            '/(tooth)$/i' => '\1teeth',
            '/(foot)$/i' => '\1feet',
            '/(person)$/i' => '\1people',
            '/(goose)$/i' => '\1geese',
            '/(mouse)$/i' => '\1mice',
            '/(cactus)$/i' => '\1cacti',
            '/(knife)$/i' => '\1knives',
            '/(leaf)$/i' => '\1leaves',
            '/(life)$/i' => '\1lives',
            '/(wife)$/i' => '\1wives',
            '/(hero)$/i' => '\1heroes',
            '/(potato)$/i' => '\1potatoes',
            '/(tomato)$/i' => '\1tomatoes',
            '/(buffalo)$/i' => '\1buffaloes',
            '/(index)$/i' => '\1indices',
            '/(alias)$/i' => '\1aliases',
            '/(status)$/i' => '\1statuses',
            '/(radius)$/i' => '\1radii',
            '/(syllabus)$/i' => '\1syllabi',
            '/(focus)$/i' => '\1foci',
            '/(fungus)$/i' => '\1fungi',
            '/(datum)$/i' => '\1data',
            '/(appendix)$/i' => '\1appendices',
            '/(bacterium)$/i' => '\1bacteria',
            '/(curriculum)$/i' => '\1curricula',
            '/^(compan)y$/i' => '\1ies', // adicionado padrão para "company" -> "companies"

        ];

        foreach ($singularEndings as $pattern => $replacement) {
            if (preg_match($pattern, $word)) {
                return preg_replace($pattern, $replacement, $word);
            }
        }

        return $word . 's';
    }


    /**
     * Delete a dir full
     *
     * @param string $path
     * @return bool
     */
    public static function deleteDirectory(string $path): bool
    {
        $path = str_replace(['/', '\\'], DIRECTORY_SEPARATOR, $path);
        if (!is_dir($path)) {
            return true;
        }

        $iterator = new \RecursiveDirectoryIterator($path, \FilesystemIterator::SKIP_DOTS);
        $rec_iterator = new \RecursiveIteratorIterator($iterator, \RecursiveIteratorIterator::CHILD_FIRST);

        foreach ($rec_iterator as $file) {
            $file->isFile() ? unlink($file->getPathname()) : rmdir($file->getPathname());
        }

        rmdir($path);
        return is_dir($path);
    }

    public static function fileSearchRecursive($file_name, $dir_init, $deep = 10)
    {
        $dirarray = explode(DIRECTORY_SEPARATOR, $dir_init);
        $filename = implode(DIRECTORY_SEPARATOR, $dirarray) . DIRECTORY_SEPARATOR . $file_name;
        $count = 0;
        while (!@file_exists($filename) && $count < $deep) { // paths acima
            array_pop($dirarray);
            $filename = implode(DIRECTORY_SEPARATOR, $dirarray) . DIRECTORY_SEPARATOR . $file_name;
            $count++;
        }
        $filename = @realpath($filename);
        if (!@file_exists($filename)) {
            return false;
        } else {
            return $filename;
        }
    }

    public static function generateCode($string)
    {
        return strtoupper(substr($string, 0, 1) . substr($string, -1) . substr(md5($string), 0, 2));
    }

    public static function shellExec($command)
    {

        $descriptorspec = [
            0 => ['pipe', 'r'], // stdin is a pipe that the child will read from
            1 => ['pipe', 'w'], // stdout is a pipe that the child will write to
            2 => ['pipe', 'w'], // stderr is a pipe that the child will write to
        ];

        $process = proc_open($command, $descriptorspec, $pipes);

        if (is_resource($process)) {
            fclose($pipes[0]); // Close the stdin pipe, since we won't be using it

            // Read the output from stdout and stderr pipes
            $output = '';
            while (($buffer = fgets($pipes[1])) !== false || ($buffer = fgets($pipes[2])) !== false) {
                $output .= $buffer;
                echo $buffer; // Display the output in real-time
                flush(); // Force the output to be sent to the browser

                // If PHP output buffering is enabled, force it to be sent to the browser
                if (ob_get_length() > 0) {
                    ob_flush();
                }
            }

            fclose($pipes[1]);
            fclose($pipes[2]);

            $return_value = proc_close($process);

            // echo "Command returned: $return_value\n";
        }
    }

    public static function commandPrintHeader($text, $size = 60)
    {
        $cmd = "header=\"$text\" && width=$size && padding=\$(((\$width - \${#header}) / 2)) && printf '%*s\n' \"\${COLUMNS:-40}\" \"\" | tr ' ' '-' | cut -c 1-\"\${width}\" && printf \"|%*s%s%*s|\n\" \$padding \"\" \"\$header\" \$padding \"\" && printf '%*s\n' \"\${COLUMNS:-80}\" \"\" | tr ' ' '-' | cut -c 1-\"\${width}\"";
        self::shellExec($cmd);
    }

    public static function copyDir($source, $destination, array $ignore = [])
    {
        // Verifica se o diretório de origem existe
        if (!file_exists($source)) {
            return false;
        }

        // Cria o diretório de destino se ele não existir
        if (!file_exists($destination)) {
            mkdir($destination, 0777, true);
        }

        // Faz um loop pelos arquivos e pastas dentro do diretório de origem
        $dir = opendir($source);
        while ($file = readdir($dir)) {
            if ($file !== '.' && $file !== '..' && array_search($file, $ignore) === false) {
                $srcFile = $source . '/' . $file;
                $destFile = $destination . '/' . $file;

                // Copia arquivos ou pastas recursivamente
                if (is_dir($srcFile)) {
                    self::copyDir($srcFile, $destFile);
                } else {
                    !file_exists($destFile) ? copy($srcFile, $destFile) : null;
                }
            }
        }
        closedir($dir);
        return true;
    }

    public static function getEnv($key)
    {
        $envFilePath = Helper::getPathApp() . '/.env';
        if (file_exists($envFilePath . '.' . getenv('APP_ENV'))) {
            $envFilePath .= '.' . getenv('APP_ENV');
        }

        $arr = [];
        if (file_exists($envFilePath)) {
            $arr = parse_ini_file($envFilePath);
            return $arr[$key] ?? null;
        } else {
            return getenv($key) !== false ? getenv($key) : null;
        }
    }
}
