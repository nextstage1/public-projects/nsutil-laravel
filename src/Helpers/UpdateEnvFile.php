<?php

namespace NsUtilLaravel\Helpers;

use NsUtilLaravel\Helpers\Helper;

class UpdateEnvFile
{
    public static function update($key, $newValue)
    {
        $envFilePath = Helper::getPathApp() . '/.env';
        if (file_exists($envFilePath . '.' . getenv('APP_ENV'))) {
            $envFilePath .= '.' . getenv('APP_ENV');
        }

        if (!file_exists($envFilePath)) {
            copy(__DIR__ . '/../../codebox/.env.example', Helper::getPathApp() . '/.env');
        }

        $contentArray = file($envFilePath, FILE_IGNORE_NEW_LINES);
        $keyFound = false;

        // Verifica se newValue contém espaços ou é uma variável referenciada
        if (strpos($newValue, ' ') !== false || strpos($newValue, '$') !== false) {
            $newValue = '"' . $newValue . '"';
        }

        foreach ($contentArray as $index => $line) {
            // Ignora linhas de comentários
            if (strpos(trim($line), '#') === 0 || empty(trim($line))) {
                continue;
            }

            if (strpos($line, $key . '=') === 0) {
                $contentArray[$index] = $key . '=' . $newValue;
                $keyFound = true;
                break;
            }
        }

        if (!$keyFound) {
            $contentArray[] = $key . '=' . $newValue;
        }

        $newContent = implode("\n", $contentArray);
        file_put_contents($envFilePath, $newContent);
    }
}
