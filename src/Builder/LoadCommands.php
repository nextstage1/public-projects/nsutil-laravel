<?php

namespace NsUtilLaravel\Builder;

use NsUtilLaravel\Helpers\Helper;

class LoadCommands
{

    public static function load(): array
    {
        return [
            Helper::getPathApp() . '/app/Console/Commands',
            realpath(__DIR__ . '/../../app/Console/Commands')
        ];
    }
}
