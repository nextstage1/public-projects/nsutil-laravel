<?php

namespace NsUtilLaravel\Builder;

use NsUtilLaravel\Helpers\Helper;
use NsUtilLaravel\Helpers\Str;
use NsUtilLaravel\Helpers\UpdateEnvFile;

class Codebox
{

    private static function checkAppEnv()
    {
        $env = Helper::getEnv('APP_ENV');
        if (null !== $env && 'local' !== $env && 'testing' !== $env) {
            echo "\n\033[31m>> Your env 'APP_ENV' is not 'local or testing'. \"[$env]\" Aborted.\033[0m\n";
            die("Aborted");
        }
    }


    public static function update()
    {

        self::checkAppEnv();

        $pathAPP =  Helper::getPathApp();

        $alwaysDefault = [
            'docker/postgres/conf.d',
            'docker/postgres/extras',
            'docker/postgres/init-scripts',
            'docker/postgres/sql',
            'docker/postgres/Dockerfile',
            'docker/scripts',
            'tests/Feature/Auth'
        ];
        array_map(
            fn ($item) => is_file("$pathAPP/$item")
                ? unlink("$pathAPP/$item")
                : Helper::deleteDirectory("$pathAPP/$item"),
            $alwaysDefault
        );


        echo "\033[34m   >> Existing files will NOT be overwritten, except for default files\033[0m\n";

        Helper::copyDir(__DIR__ . '/../../codebox/', $pathAPP . '/', ['.backup', 'tag.sh']);

        shell_exec("chmod 0777 $pathAPP -R");
    }

    public static function config()
    {

        self::checkAppEnv();

        $pathAPP =  Helper::getPathApp();

        // check if .env is correct configured
        if (Helper::getEnv('CODEBOX_INSTALLED') != "true") {
            echo "\033[32m   >> Updated initial configurations!\033[0m\n";

            // Update composer.json
            $actualComposer = json_decode(file_get_contents("$pathAPP/composer.json"), true);
            $appName = explode('/', $actualComposer['name'])[1];

            # update envs
            $dbname = str_replace(['-', '_', ' '], '_', $appName) . '_db';
            UpdateEnvFile::update('APP_KEY', Str::random(64));
            UpdateEnvFile::update('APP_NAME', $appName);
            UpdateEnvFile::update('APP_PORT_EXPOSED', rand(8005, 9005));
            UpdateEnvFile::update('COMPOSE_PROJECT_NAME', $appName);
            UpdateEnvFile::update('PG_EXPOSED_PORT', rand(16000, 19000));
            UpdateEnvFile::update('DB_DATABASE', $dbname);
            UpdateEnvFile::update('PG_MULTIPLE_DATABASES', $dbname);
            UpdateEnvFile::update('CODEBOX_INSTALLED', '"true"');
        } else {
            echo "\033[32m   >> No update is necessary.\033[0m\n";
        }

        // check if JWT is configured
        if (!Helper::getEnv('JWT_SECRET') || Helper::getEnv('JWT_SECRET') === '') {
            UpdateEnvFile::update('JWT_SECRET', Str::random(64));
        }
    }
}
