<?php

namespace NsUtilLaravel\Builder;

use Exception;
use Illuminate\Contracts\Filesystem\FileNotFoundException;
use NsUtilLaravel\Builder\Creators\Postman;
use NsUtilLaravel\Builder\Data;
use NsUtilLaravel\Helpers\ConfigPersisted;
use NsUtilLaravel\Helpers\ConsoleTable;
use NsUtilLaravel\Helpers\Helper;
use SebastianBergmann\Diff\ConfigurationException;

class Builder
{

    public static $config = [];

    public static function run()
    {

        $fileconfig = Helper::getPathApp() . '/.build.config.json';
        if (!file_exists($fileconfig)) {
            throw new FileNotFoundException("ERROR: File $fileconfig not found");
        }

        $config = self::getConfig();

        Helper::commandPrintHeader('Botschauer RESTifier');

        $data = new Data();
        self::setNamespaceToModel($config, $data);
        // var_export($data->getData()->namespaces);
        $tables = array_values(array_map(fn ($item) => $item->table, (array) $data->getData()->items));

        // Remover arquivos mapeados como sempre default
        self::alwaysDefault($config->alwaysDefault);

        // Inicar a documentacao postman
        $postman = new ConfigPersisted('postman');
        $initial = json_decode(file_get_contents(__DIR__ . '/Creators/Postman/collection.json'), true);
        // unset($initial['info']);
        $postman->init(['collection' => $initial]);

        // Builder
        $creators = [
            \NsUtilLaravel\Builder\Creators\Request::class,
            \NsUtilLaravel\Builder\Creators\Postman::class,
            // \NsUtilLaravel\Builder\Creators\Migration::class,
            \NsUtilLaravel\Builder\Creators\Provider::class,
            \NsUtilLaravel\Builder\Creators\Model::class,
            \NsUtilLaravel\Builder\Creators\Resource::class,
            \NsUtilLaravel\Builder\Creators\Controller::class,
            \NsUtilLaravel\Builder\Creators\Router::class,
            \NsUtilLaravel\Builder\Creators\Translate::class,
            \NsUtilLaravel\Builder\Creators\Tests::class,
            \NsUtilLaravel\Builder\Creators\Factory::class,
            \NsUtilLaravel\Builder\Creators\Seeder::class,
            \NsUtilLaravel\Builder\Creators\VerifyBelongToRule::class,
            \NsUtilLaravel\Builder\Creators\RulesJsonPrepared::class,
        ];

        // Cabecalho da saida
        $console = new ConsoleTable();
        $console->addHeader('Module');
        array_map(fn ($item) => $console->addHeader(
            str_replace('::class', '', basename(str_replace('\\', '/', $item)))
        ), $creators);

        // Rodar pelos modules
        $filesTotal = 0;
        foreach ($config->modules as $item) {
            echo "\n";
            echo "\r- Module $item->module: ";
            $line = [$item->module];
            $data->set('module', ucwords(Helper::name2CamelCase($item->module)));

            // Ignore routers
            $ignoreRoutersModule = array_merge(
                (array) ($item->ignoreRouters ?? []),
                [
                    'companies' => array_merge($ignoreRouters['companies'] ?? [], ['store', 'destroy']),
                    'users' => array_merge($ignoreRouters['users'] ?? [], ['store', 'destroy']),
                    'files' => array_merge($ignoreRouters['files'] ?? [], ['update'])
                ]
            );

            // Ignore Controllers
            $ignoreController = array_merge(
                array_values(array_filter($tables, fn ($i) => array_search($i, $item->tables) === false)),
                ['failed_jobs', 'migrations', 'password_reset_tokens', 'personal_access_tokens', 'jwt_tokens', 'spatial_ref_sys']
            );

            // Execution in module
            $moduleTotalFiles = 0;
            foreach ($creators as $key => $creatorClass) {
                $creatorInstance = new $creatorClass();

                if ($creatorInstance instanceof \NsUtilLaravel\Builder\Creators\Router) {
                    $files = $creatorInstance->handle($data, $ignoreController, $ignoreRoutersModule);
                } else {
                    $files = $creatorInstance->handle($data, $ignoreController);
                }
                $line[] = $files . ' files';
                $moduleTotalFiles += $files;

                // Execution
                echo "\r- Module $item->module: " . round(($key + 1) / count($creators) * 100) . '%';
            }
            $filesTotal += $moduleTotalFiles;
            echo "\r- Module $item->module: $moduleTotalFiles files";
            $console->addRow($line);
        }

        // gerar documentação postman
        $saved = Helper::getPathApp() . '/.postman.collection.json';
        Helper::saveFile(
            $saved,
            str_replace(
                [
                    'API-NAME',
                    'http://localhost:8000/api'
                ],
                [
                    getenv('APP_NAME') . ' - API',
                    "http://localhost:" . getenv('APP_PORT_EXPOSED') . "/api"
                ],
                json_encode(
                    $postman->get()['collection'],
                    JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE
                )
            ),
            'SOBREPOR'
        );
        $postman->remove();

        // Gerar o HTML
        echo "\n- Generate HTML from API Documentation:";
        echo str_replace('Generating output... ', ' ', Postman::generateHTML($saved));

        echo "\n";
        echo "\nTotal: $filesTotal files in modules";
        echo "\n";
        // $console->display();
        echo "\n ";
    }


    private static function alwaysDefault($files): void
    {
        // Remover os arquivos enviados como padrão
        $files[] = '/app/Generated';
        array_map(function ($item) {
            $item = Helper::getPathApp()
                . DIRECTORY_SEPARATOR
                . ($item[0] === '/' ? substr($item, 1) : $item);

            if (is_dir($item)) {
                Helper::deleteDirectory($item);
            } elseif (file_exists($item)) {
                unlink($item);
            } else {
                return false;
            }
            return true;
        }, $files);
    }

    private static function getConfig($extras = []): object
    {
        $fileconfig = Helper::getPathApp() . '/.build.config.json';
        if (!file_exists($fileconfig)) {
            throw new FileNotFoundException("ERROR: File $fileconfig not found");
        }
        $config = json_decode(file_get_contents($fileconfig), true);

        // Defaults
        $config['modules'][] =  [
            "module" => "Company",
            "ignoreRouters" => [
                "companies" => [
                    "store",
                    "destroy"
                ],
                "permissions" => true,
                "users" => [
                    "store",
                    "destroy"
                ],
                "user_profile" => ["update"]
            ],
            "tables" => [
                "companies",
                "permissions",
                "profile_permissions",
                "profiles",
                "user_profile",
                "users",
                "invites"
            ]
        ];

        $config['modules'][] =   [
            "module" => "Subscription",
            "tables" => [
                "plans",
                "subscriptions"
            ]
        ];
        $config['modules'][] = [
            "module" => "Application",
            "tables" => [
                "files",
                "addresses",
                "webhooks",
                "queues"
            ],
            "ignoreRouters" => [
                'webhooks' => ['store', 'destroy'],
                'files' => true,
                'queues' => true
            ]
        ];

        $config = json_decode(json_encode($config));


        // validate
        $error = 'ERROR: The configuration file is invalid: ';
        switch (true) {
            case !isset($config->alwaysDefault) || !is_array($config->alwaysDefault):
                throw new Exception($error . "key alwaysDefault is not found or not is an array");
                break;
            case !isset($config->modules) || !is_array($config->modules):
                throw new Exception($error . "key 'modules' is not found or not is an array");
                break;
            case !isset($config->modules[0]->module):
                throw new Exception($error . "key 'modules->module' is not found");
                break;
            case !isset($config->modules[0]->tables) || !is_array($config->modules[0]->tables):
                throw new Exception($error . "key 'modules->tables' is not found or not is an array");
                break;
        }

        self::$config = $config;
        return $config;
    }

    /**
     * Ira aplicar o namespace nos dados obtidos por data e conforme configurações
     *
     * @param object $config
     * @param Data $data
     * @return void
     */
    private static function setNamespaceToModel(object $config, Data $data): void
    {
        // echo "\n- Preparing namespaces";
        $reference = [];
        foreach ($data->getData()->items as $key => $item) {
            // echo $item->table . ": ";
            $module = array_values(
                array_filter(
                    $config->modules,
                    fn ($i) => array_search($item->table, $i->tables) !== false
                )
            );

            if (isset($module[0])) {
                $reference[$item->table] = $module[0]->module;
            }
            // echo PHP_EOL;
        }
        $data->set('namespaces', $reference);
    }
}
