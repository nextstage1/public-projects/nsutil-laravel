<?php


namespace NsUtilLaravel\Builder\Creators;

use NsUtilLaravel\Builder\Data;
use NsUtilLaravel\Builder\LoadDB;
use NsUtilLaravel\Helpers\Helper;
use NsUtilLaravel\Helpers\Template;

class Model implements CreateInterface
{

    private $templateUserModel = "<?php

    namespace App\Generated\Modules\{module}\Models;
    
    use Illuminate\Database\Eloquent\Factories\HasFactory;
    use Illuminate\Foundation\Auth\User as Authenticatable;
    use Illuminate\Notifications\Notifiable;
    use Illuminate\Database\Eloquent\SoftDeletes;
    use Illuminate\Database\Eloquent\Prunable;
    use Tymon\JWTAuth\Contracts\JWTSubject;
    use App\Modules\Company\Database\Factories\UserFactory;
    
    class AbstractUser extends Authenticatable implements JWTSubject
    {
        use HasFactory, Notifiable,  SoftDeletes, Prunable;

        protected \$primaryKey = '{primaryKey}';
    
        /**
         * The attributes that are mass assignable.
         *
         * @var array<int, string>
         */
        protected \$fillable = [
            'name',
            'email',
            'password',
        ];
    
        /**
         * The attributes that should be hidden for serialization.
         *
         * @var array<int, string>
         */
        protected \$hidden = [
            'password',
            'remember_token',
        ];
    
        /**
         * The attributes that should be cast.
         *
         * @var array<string, string>
         */
        protected \$casts = [
            'email_verified_at' => 'datetime',
        ];
    
    
        public function getJWTIdentifier()
        {
            return \$this->getKey();
        }
    
        public function getJWTCustomClaims()
        {
            return [];
        }
    
        public function prunable()
        {
            return static::where('deleted_at', '<=', now()->subMonth());
        }

        protected static function newFactory(): UserFactory
        {
            return new UserFactory();
        }
    }
    ";


    private $template = "<?php

    namespace App\Generated\Modules\{module}\Models;
    
    {softDeletesA}

    use App\Modules\{module}\Database\Factories\{model}Factory;
    
    abstract class Abstract{model} extends Model
    {
        {softDeletesB}

        protected \$table = '{table}';
        protected \$fillable = [{fillable}];
        protected \$casts = [{casts}];

        {relations}

        {prunable_function}

        protected static function newFactory(): {model}Factory
        {
            return new {model}Factory();
        }
    }
    ";

    private $templateModule = "<?php

    namespace App\Modules\{module}\Models;

    use App\Generated\Modules\{module}\Models\Abstract{model};
    
    final class {model} extends Abstract{model}
    {
    }
    ";

    private function getBelongTo($fnName, $model)
    {
        return "public function $fnName()
        {
            return \$this->belongsTo($model::class);
        }
        ";
    }

    private function getHasMany($fnName, $model)
    {
        return "public function $fnName()
        {
            return \$this->hasMany($model::class);
        }
        ";
    }

    public function handle(Data $data, array $ignore = []): int
    {
        $module = $data->getData()->module;

        // Na model, vou criar o arquivo "config" da model
        Helper::saveFile(
            Helper::getPathApp() . '/app/Modules/' . $module . '/Config/' . mb_strtolower($module) . '.php',
            "<?php\n\n return [];",
            Helper::IF_NOT_UPDATE
        );


        // $pathToSave = Helper::getPathApp() . '/app/Modules/' . $data->getData()->module . '/Models';

        $update = [
            'prunable_function' => '',
            'softDeletesA' => '    
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
',
            'softDeletesB' => 'use HasFactory;'
        ];
        // echo "- Models: ";

        foreach ($data->getData()->items as $item) {
            if (array_search($item->table, $ignore) !== false) {
                continue;
            }

            // // Especificamente para Users
            // if ($item->table === 'users') {
            //     $update = array_merge($update, [
            //         'module' => $data->getData()->module,
            //         'table' => $item->table,
            //         'model' => $item->model,
            //     ]);


            //     (new Template($this->templateUserModel, $update))
            //         ->renderTo($pathToSave . '/' . $item->model . '.php', Helper::IF_NOT_UPDATE);
            //     continue;
            // }

            $update = [
                'prunable_function' => '',
                'softDeletesA' => '    
    use Illuminate\Database\Eloquent\Factories\HasFactory;
    use Illuminate\Database\Eloquent\Model;
    ',
                'softDeletesB' => 'use HasFactory;'
            ];

            $tableItens = ['relations' => [], 'fillable' => [], 'casts' => []];


            foreach ($item->hasMany as $hasMany) {
                $tableItens['relations'][$hasMany->table] = $this->getHasMany(
                    Helper::name2CamelCase($hasMany->table),
                    HelperCreator::getNameSpace($data, $hasMany->table, '\\Models\\') . $hasMany->model
                );
            }


            $primaryKey = 'id';
            foreach ($item->fields as $field) {
                if (!isset($field->type)) {
                    $tableItens['relations'][] = $this->getBelongTo(
                        lcfirst($field->model),
                        HelperCreator::getNameSpace($data, $field->table, '\\Models\\') . $field->model
                    );
                    $tableItens['fillable'][] = $field->field_origin;
                    continue;
                }
                if (
                    $field->isPrimaryKey
                    || stripos($field->column_name, 'createtime') !== false
                    || stripos($field->column_name, 'created_at') !== false
                    || stripos($field->column_name, 'updated_at') !== false
                    || stripos($field->type, 'tsvector') !== false
                ) {
                    continue;
                }
                $primaryKey =  $field->isPrimaryKey ? $field->column_name : $primaryKey;

                // softdelete
                if (stripos($field->column_name, 'deleted_at') !== false) {
                    $update['prunable_function'] = "
                    public function prunable()
                    {
                        return static::where('deleted_at', '<=', now()->subMonth());
                    }
                    ";
                    $update['softDeletesA'] = '    
                        use Illuminate\Database\Eloquent\Factories\HasFactory;
                        use Illuminate\Database\Eloquent\Model;
                        use Illuminate\Database\Eloquent\SoftDeletes;
                        use Illuminate\Database\Eloquent\Prunable;
                        ';
                    $update['softDeletesB'] = 'use HasFactory, SoftDeletes, Prunable;';
                    continue;
                }



                $tableItens['fillable'][] = $field->column_name;

                // casts
                if (LoadDB::$config[$field->type][0] === 'json') {
                    $tableItens['casts'][] = "'" . $field->column_name . "' => 'json'";
                }
            }

            $update = array_merge($update, [
                'casts' => implode(",", $tableItens['casts']),
                'primaryKey' => $primaryKey,
                'module' => $module,
                'table' => $item->table,
                'model' => $item->model,
                'fillable' => implode(",", array_map(fn ($item) => "'$item'", $tableItens['fillable'])),
                'relations' => implode("\n", array_map(fn ($item) => $item, $tableItens['relations'])),
            ]);

            $me = explode('\\', __CLASS__);
            $include = __DIR__
                . '/Templates/'
                . end($me)
                . '/'
                . $item->model
                . '.php';

            $template = file_exists($include)
                ? include $include
                : $this->template;

            SaveFile::save(self::class, $module,  $item->model, new Template(
                $template,
                $update
            ), new Template($this->templateModule, $update));
        }

        return SaveFile::countGeneratedFiles(self::class, $module);
    }
}
