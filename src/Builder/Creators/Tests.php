<?php

namespace NsUtilLaravel\Builder\Creators;

use NsUtilLaravel\Builder\Data;
use NsUtilLaravel\Helpers\Helper;
use NsUtilLaravel\Helpers\Template;
use NsUtilLaravel\Helpers\TopologicalSort;

class Tests
{

    private $template = "<?php

    uses(Illuminate\Foundation\Testing\RefreshDatabase::class);
    
    use Illuminate\Support\Str;
    use Tymon\JWTAuth\Facades\JWTAuth;
    use App\Modules\{module}\Models\{model} as ModelTest;
    use Illuminate\Support\Facades\Hash;
    
    beforeEach(function () {
        \$this->company = \App\Modules\Company\Models\Company::factory()->create([
            'name' => 'Testes Unitarios'
        ]);
    
        \$profile = \App\Modules\Company\Models\Profile::factory()->create([
            'name' => 'Testes Unitarios',
            'company_id' => \$this->company->id, 
            'is_root' => true
        ]);
    
        \$this->user = \App\Modules\Company\Models\User::factory()->create([
            'name' => 'Tester PhpPest',
            'email' => 'tester-pest@local.com', 
            'password' => Hash::make('1234567890')
        ]);
    
        \$this->company_user = \App\Modules\Company\Models\UserProfile::factory()->create([
            'user_id' => \$this->user->id,
            'profile_id' => \$profile->id,
        ]);

        // Login to get token
        \$modelLogin = [
            'email' => \$this->user->email,
            'password' => '1234567890',
            'company_id' => \$this->company->id
        ];
        \$response = \$this->postJson('/api/auth/login', \$modelLogin);
        \$this->token = \$response->json('data.access_token');

        // Exclusive to test
        {relationsCreator}
    
    });


    {tests}
    ";

    private function getCanCreate()
    {
        return "
        test('can create a {model_camelcase}', function ()  {
            \$data = ['company_id' => \$this->company->id];
            \$modelData = ModelTest::factory()->make(\$data)->toArray();        
            \$response = \$this->withHeaders(['Authorization' => 'Bearer ' . \$this->token])->postJson('/api/{route}', \$modelData);

            // See errors
            \$error = \$response->json('meta.error');
            if (false !== \$error) {
                echo PHP_EOL;
                echo '--- Error ---';
                echo PHP_EOL;
                echo json_encode([
                    'test' => \$this->getName(),
                    'payload' => \$modelData,  
                    'meta' => \$response->json('meta')
                ], JSON_PRETTY_PRINT);
                echo PHP_EOL;
                die();
            }

            \$response->assertStatus(201);
            \$response->assertJsonStructure([
                'data' => [{fields}]
            ]);
        });";
    }

    private function getCanUpdate()
    {
        return "
        test('can update a {model_camelcase}', function ()  {
            \$model = ModelTest::factory()->create();
            \$updatedData = ModelTest::factory()->make(['company_id' => \$this->company->id])->toArray();
            \$response = \$this->withHeaders(['Authorization' => 'Bearer ' . \$this->token])->putJson('/api/{route}/' . \$model->id, \$updatedData);

            // See errors
            \$error = \$response->json('meta.error');
            if (false !== \$error) {
                echo PHP_EOL;
                echo '--- Error ---';
                echo PHP_EOL;
                echo json_encode([
                    'test' => \$this->getName(),
                    'payload' => \$updatedData ?? [],  
                    'meta' => \$response->json('meta')
                ], JSON_PRETTY_PRINT);
                echo PHP_EOL;
                die();
            }

            \$response->assertStatus(200);
            \$response->assertJsonStructure([
                'data' => [{fields}]
            ]);
        });
        ";
    }

    private function getCanDelete()
    {
        return "
        test('can delete a {model_camelcase}', function ()  {
            \$model = ModelTest::factory()->create();
            \$response = \$this->withHeaders(['Authorization' => 'Bearer ' . \$this->token])->deleteJson('/api/{route}/' . \$model->id);
            
            // See errors
            \$error = \$response->json('meta.error');
            if (false !== \$error) {
                echo PHP_EOL;
                echo '--- Error ---';
                echo PHP_EOL;
                echo json_encode([
                    'test' => \$this->getName(),
                    'payload' => \$model->id,  
                    'meta' => \$response->json('meta')
                ], JSON_PRETTY_PRINT);
                echo PHP_EOL;
                die();
            }


            \$response->assertStatus(204);
        });
        ";
    }

    private function getCanList()
    {
        return "
        test('can list {model_camelcase}', function ()  {
            ModelTest::factory()->count(1)->create();
            \$response = \$this->withHeaders(['Authorization' => 'Bearer ' . \$this->token])->getJson('/api/{route}');
            
            // See errors
            \$error = \$response->json('meta.error');
            if (false !== \$error) {
                echo PHP_EOL;
                echo '--- Error ---';
                echo PHP_EOL;
                echo json_encode([
                    'test' => \$this->getName(),
                    'meta' => \$response->json('meta')
                ], JSON_PRETTY_PRINT);
                echo PHP_EOL;
                die();
            }

            \$response->assertStatus(200);
            \$response->assertJsonStructure([
                'data' => ['*' => [{fields}]]
            ]);
        });
        ";
    }

    private function getCanRead()
    {
        return "
        test('can read a {model_camelcase}', function ()  {
            \$model = ModelTest::factory()->create();
            \$response = \$this->withHeaders(['Authorization' => 'Bearer ' . \$this->token])->getJson('/api/{route}/' . \$model->id);
            
            // See errors
            \$error = \$response->json('meta.error');
            if (false !== \$error) {
                echo PHP_EOL;
                echo '--- Error ---';
                echo PHP_EOL;
                echo json_encode([
                    'test' => \$this->getName(),
                    'model' => \$model->toArray(),
                    'meta' => \$response->json('meta')
                ], JSON_PRETTY_PRINT);
                echo PHP_EOL;
                die();
            }


            \$response->assertStatus(200);
            \$response->assertJsonStructure([
                'data' => [{fields}]
            ]);
        });
        ";
    }


    public function handle(Data $data, $ignore = []): int
    {
        $pathToSave = Helper::getPathApp() . '/app/Modules/' . $data->getData()->module . '/Tests/Feature/Generated';
        $pathToSave = Helper::getPathApp() . '/tests/Feature/Modules/' . $data->getData()->module;

        foreach ($data->getData()->items as $item) {

            if (array_search($item->table, $ignore) !== false) {
                continue;
            }

            // Tests default
            $default = __DIR__ . "/TestDefault/$item->model.txt";
            if (file_exists($default)) {
                (new Template($default, []))
                    ->renderTo($pathToSave . '/' . $item->model . 'Test.php', Helper::IF_NOT_UPDATE);
                continue;
            }


            $top = [];
            $tables = [];
            foreach (array_filter(
                (array) $item->fields,
                fn ($item) => isset($item->field_origin) && $item->model !== 'Company'
            ) as $filtered) {
                $top[$item->table][] = $filtered->table;
                $tables[] = $filtered->table;
            }

            // Caminho unico para chegar até company
            $routers = HelperCreator::getRoutes($data, $item->table, /*includeNullable=*/ true);
            if (count($routers['router']) > 0) {
                foreach ($routers['router'] as $router) {
                    $relations = explode('.', $router);
                    array_pop($relations);
                    list($table, $cpoID, $cpoRel) = explode('|', end($relations));
                    $lastTable = $item->table;
                    foreach ($relations as $relationItem) {
                        if (stripos($relationItem, '|') !== false) {
                            list($table, $cpoID, $cpoRel) = explode('|', $relationItem);
                            $top[$lastTable][] = $table;
                            $lastTable = $table;
                            $tables[] = $lastTable;
                            $tables[] = $table;
                        }
                    }
                }
            }

            // montar a tree das tabelas relacionadas
            $ignoreRelations = ['companies', 'profiles', 'users', 'user_profile', 'migrations'];
            $tables = array_diff(array_unique($tables), $ignoreRelations);
            $relations = $data->getData()->relations;
            $tree = [];
            foreach ($relations as $ref => $rels) {
                if (array_search($ref, $tables) !== false) {
                    $tree[$ref] = array_map(
                        fn ($item) => $item->referenced_table_name,
                        array_filter(
                            (array)$rels,
                            // ignorar tabelas já criadas no test
                            fn ($item) => array_search($item->referenced_table_name, $ignoreRelations) === false
                        )
                    );
                }
            }

            $relationsCreator = [
                'router' => array_map(
                    fn ($table) =>
                    HelperCreator::getNameSpace($data, $table, '\\Models\\')
                        . ucwords(Helper::name2CamelCase(Helper::singularize($table)))
                        . '::factory()->create();',
                    array_filter(TopologicalSort::sort($tree), fn ($t) => $t !== $item->table)
                )
            ];

            // if ($item->table === 'products') {
            // dd($tables, $tree, $relationsCreator);
            // }


            $fieldsToIgnore = ['created_at', 'updated_at', 'deleted_at', 'company_id'];
            $fields = array_map(
                fn ($item) => "'" . ($item->column_name ?? $item->field_origin) . "'",
                array_filter(
                    (array) $item->fields,
                    fn ($item) => isset($item->column_name) && array_search(($item->column_name ?? $item->field_origin), $fieldsToIgnore) === false
                )
            );
            $router = str_replace('_', '-', Helper::reverteName2CamelCase($item->model));
            $update = [
                'relationsCreator' => implode('', $relationsCreator['router']),
                'module' => $data->getData()->module,
                'table' => $item->table,
                'model' => $item->model,
                'model_camelcase' => trim(lcfirst($item->model)),
                'fields' => implode(',', $fields),
                'route' => $router
            ];

            $this->template = str_replace('{tests}', implode("\n", [
                $this->getCanCreate(),
                $this->getCanRead(),
                $this->getCanList(),
                $this->getCanUpdate(),
                $this->getCanDelete()
            ]), $this->template);

            (new Template($this->template, $update))
                ->renderTo($pathToSave . '/' . $item->model . 'Test.php', Helper::IF_NOT_UPDATE);
        }

        return is_dir($pathToSave) ? count(scandir($pathToSave)) - 2 : 0;
    }
}
