<?php

namespace NsUtilLaravel\Builder\Creators;

use NsUtilLaravel\Builder\Creators\CreateInterface;
use NsUtilLaravel\Builder\Data;
use NsUtilLaravel\Helpers\Helper;
use NsUtilLaravel\Helpers\Template;

class Provider implements CreateInterface
{
    private $template = "<?php

    namespace App\Generated\Modules\{module}\Providers;
    
    use Illuminate\Support\ServiceProvider;
    use Illuminate\Database\Eloquent\Factories\Factory as EloquentFactory;
    use Illuminate\Database\Seeder;
    use Illuminate\Support\Facades\Route;
    use Faker\Factory as FakerFactory;
    
    abstract class Abstract{module}ServiceProvider extends ServiceProvider
    {
    
        public function register()
        {
            // Não é necessário registrar a fábrica aqui.
        }
    
        public function boot()
        {
            // OK!
            \$apiPath = base_path().'/app/Modules/{module}/Routes/api.php';
            if (file_exists(\$apiPath)){
                Route::prefix('api')->group(fn () => \$this->loadRoutesFrom(\$apiPath));
            }

            \$this->app['router']->middleware('{module}.check.belong.to', \App\Modules\{module}\Http\Middlewares\CheckBelongTo::class);
    
            // OK!
            \$this->loadMigrationsFrom([
                base_path() . '/app/Modules/{module}/Database/Migrations',
                //base_path() . '/app/Generated/Modules/{module}/Database/Migrations'
            ]);
        
            // \$this->loadViewsFrom(base_path().'/app/Modules/{module}/Views', '{module_lower}');
    
            \$this->publishes([
                base_path().'/app/Modules/{module}/Config/{module_lower}.php' => config_path('{module_lower}.php'),
            ], '{module_lower}-config');
        }
    }
    ";

    private $templateModule = "<?php

    namespace App\Modules\{module}\Providers;

    use App\Generated\Modules\{module}\Providers\Abstract{module}ServiceProvider;

    final class {module}ServiceProvider extends Abstract{module}ServiceProvider
    {
    }   
    ";

    public function handle(Data $data, $ignore = []): int
    {
        // echo "\n- Provider: ";
        $update = [
            'module' => $data->getData()->module,
            'module_lower' => mb_strtolower($data->getData()->module),
        ];
        $module = $data->getData()->module;

        SaveFile::save(self::class, $module,  $module, new Template($this->template, $update), new Template($this->templateModule, $update));


        return SaveFile::countGeneratedFiles(self::class, $module);
    }
}
