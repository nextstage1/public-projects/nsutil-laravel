<?php


namespace NsUtilLaravel\Builder\Creators;

use NsUtilLaravel\Builder\Data;
use NsUtilLaravel\Builder\LoadDB;
use NsUtilLaravel\Helpers\Helper;
use NsUtilLaravel\Helpers\Template;

class Translate implements CreateInterface
{

    public function handle(Data $data, array $ignore = []): int
    {

        $pathToSave = Helper::getPathApp() . '/app/Modules/' . $data->getData()->module . '/lang/en.json';
        $data->getData()->aliasesFields;
        $dataConfig = array_merge(
            json_decode(json_encode($data->getData()->aliasesFields), true) ?? [],
            file_exists($pathToSave) ? json_decode(file_get_contents($pathToSave), true) ?? [] : []
        );
        Helper::saveFile($pathToSave, json_encode($dataConfig, JSON_UNESCAPED_SLASHES), Helper::IF_NOT_UPDATE);

        return 1;
    }
}
