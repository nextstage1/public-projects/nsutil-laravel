<?php

namespace NsUtilLaravel\Builder\Creators;

use NsUtilLaravel\Builder\Data;

interface CreateInterface
{
    public function handle(Data $data): int;
}
