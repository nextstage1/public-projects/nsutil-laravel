<?php


namespace NsUtilLaravel\Builder\Creators;

use NsUtilLaravel\Builder\Data;
use NsUtilLaravel\Builder\LoadDB;
use NsUtilLaravel\Helpers\Helper;
use NsUtilLaravel\Helpers\Template;

class Controller implements CreateInterface
{

    private $template =    '';
    private $templateModule = "<?php

    namespace App\Modules\{module}\Http\Controllers;

    use App\Generated\Modules\{module}\Http\Controllers\Abstract{model}Controller;

    /**
     * Class {model}Controller
     *
     * This class extends the generated class. Update this to resolve the requirements of this application module.
     */
    final class {model}Controller extends Abstract{model}Controller
    {
    }    
    ";


    public function handle(Data $data, array $ignore = []): int
    {
        // $pathToSave = Helper::getPathApp() . '/app/Modules/' . $data->getData()->module . '/Http/Controllers';

        $module = $data->getData()->module;
        // echo "- Controller: ";
        $typesNotSortable = ['text', 'json', 'jsonb', 'bool', 'boolean'];
        $fieldsNotSortable = ['deleted_at'];


        foreach ($data->getData()->items as $item) {
            $searchable = $sortable = $joins = [];
            $conditions = [];
            $companyIdIsFinded = false;
            if (array_search($item->table, $ignore) !== false) {
                continue;
            }
            $tableItens = ['relations' => [], 'items' => [], 'loaders' => []];


            // Caminho unico para chegar até company
            $routers = HelperCreator::getRoutes($data, $item->table);
            // echo "\n >>> $item->table <<<\n";
            if (count($routers['router']) > 0) {
                $baseCondition = null;
                foreach ($routers['router'] as $router) {
                    $relations = explode('.', $router);
                    array_pop($relations);
                    list($table, $cpoID, $cpoRel) = explode('|', end($relations));


                    // verificar se o campo em questão é nullable
                    // $reversed = array_reverse($relations);


                    $rels = array_values(array_filter($relations, fn ($item) => $item !== 'companies'));
                    // var_export($rels);
                    $last = explode('|', array_pop($rels));
                    $prev = explode('|', array_pop($rels));
                    $tbl = strlen($prev[0]) > 0 ? $prev[0] : $item->table;
                    // echo $tbl;
                    $check = HelperCreator::checkIfFieldIsNullable($data, $tbl, $last[2]);
                    // echo "------------ $prev[0], $last[2]\n";

                    if ($check->exists) {
                        if ($check->nullable === false) {
                            $conditions["$table.company_id"] = "\$this->conditions['$table.company_id'] = (int) \$request->get('jwt_company_id');";
                        }
                    } else {
                        $conditions["$table.company_id"] = "\$this->conditions['$table.company_id'] = (int) \$request->get('jwt_company_id');";
                    }

                    // $check = HelperCreator::checkIfFieldIsNullable($data, $item->table, $cpoRel);
                    // // echo $check->nullable;
                    // if ($check->exists) {
                    //     if ($check->nullable === false) {
                    //         $conditions["$table.company_id"] = "\$this->conditions['$table.company_id'] = (int) \$request->get('jwt_company_id');";
                    //     }
                    // } else {
                    //     var_export($relations);
                    //     $conditions["$table.company_id"] = "\$this->conditions['$table.company_id'] = (int) \$request->get('jwt_company_id');";
                    // }


                    // $conditions[] = "\$this->conditions['$table.company_id'] = (int) \$request->get('jwt_company_id');";
                    $lastTable = $item->table;

                    foreach ($relations as $relationItem) {
                        if (stripos($relationItem, '|') !== false) {
                            list($table, $cpoID, $cpoRel) = explode('|', $relationItem);
                            if (!isset($joins[$table])) {
                                $check = HelperCreator::checkIfFieldIsNullable($data, $item->table, $cpoRel);
                                $joinType = $check->exists && $check->nullable ? 'leftJoin' : 'join';
                                $joins[$table] = "->$joinType('$table', '$table.$cpoID', '=', '$lastTable.$cpoRel')";
                            }
                            $tableSingular = Helper::name2KebabCase(Helper::singularize($table));
                            $filter = Helper::singularize($table) . '_id';
                            $conditions[$tableSingular] = "\$this->conditions['$table.$cpoID'] = \$request->input('$filter') ?? \$request->route('$tableSingular') ?? null;  ";
                            $lastTable = $table;
                        }
                    }
                }
            }


            foreach ($item->hasMany as $hasMany) {
                // $tableItens['loaders'][$hasMany->table] = "'" . Helper::name2CamelCase($hasMany->table) . "'";
            }

            foreach ($item->fields as $field) {
                if (!isset($field->type)) {
                    if (!isset($conditions['company_id']) && $field->field_origin === 'company_id') {
                        $tbl = $field->table === 'companies' ? $item->table : $field->table;
                        $conditions['company_id'] =
                            "\$this->conditions['$tbl.company_id'] = (int) \$request->get('jwt_company_id');";
                        $companyIdIsFinded = true;
                    }

                    // Join com a relação direta desta tabela
                    if (!isset($joins[$field->table]) && $field->model !== 'Company') {
                        $joinType = $field->nullable ? 'leftJoin' : 'join';
                        $joins[$field->table] = "->$joinType('$field->table', '$field->table.$field->field_relation', '=', '$item->table.$field->field_origin')";
                    }

                    if ($field->model !== 'Company') {
                        $tableSingular = Helper::name2KebabCase(Helper::singularize($field->table));
                        $filter = Helper::singularize($field->table) . '_id';
                        $conditions[$tableSingular] = "\$this->conditions['{$field->table}.id'] = \$request->input('$filter') ?? \$request->route('$tableSingular') ?? null;  ";
                    }


                    $tableItens['relations_relation'][$field->field_origin] = '$' . "{$field->model}List = \App\\Http\\Modules\\" . $module . "\\Resources\\{$field->model}Resource::collection(\$this->$field->model);";
                    $tableItens['items_relation'][$field->field_origin] = '$' . "{$field->model}List";

                    if ($field->model !== 'Company') {
                        $tableItens['loaders'][lcfirst($field->model)] = "'" . lcfirst($field->model) . "'";
                    }
                    continue;
                }

                // sortable
                if (
                    array_search(LoadDB::$config[$field->type][1], $typesNotSortable) === false
                    && array_search($field->column_name, $fieldsNotSortable) === false
                    && stripos($field->type, 'tsvector') === false
                ) {
                    $sortable[] = "'$field->column_name'";
                }

                // searchable
                if (LoadDB::$config[$field->type][1] === 'string' || LoadDB::$config[$field->type][1] === 'text') {
                    $searchable[]  = "'$field->column_name'";
                }


                if ($field->isPrimaryKey || stripos($field->column_name, 'createtime') !== false || stripos($field->column_name, 'created_at') !== false) {
                    continue;
                }

                // items
                $tableItens['items'][$field->column_name] = '$this->' . $field->column_name;
            }

            $tableItens['relations'] = array_merge($tableItens['relations'], ($tableItens['relations_relation'] ?? []));
            $tableItens['items'] = array_merge($tableItens['items'], ($tableItens['items_relation'] ?? []));

            // Propria Company
            if ($item->model === 'Company') {
                $conditions[] = "\$this->conditions['id'] = (int) \$request->get('jwt_company_id');";
            }
            // Proprio usuario
            if ($item->model === 'User') {
                $conditions[] = "\$this->conditions['id'] = (int) \Illuminate\Support\Facades\Auth::user()->id;";
            }


            $update = [
                'construct' => '',
                'sortable' => implode(", ", $sortable),
                'searchable' => implode(", ", $searchable),
                'joins' => implode("\n", $joins),
                'conditions' => implode("\n\n", $conditions),
                'module' => $module,
                'table' => $item->table,
                'model' => $item->model,
                'route' => Helper::name2KebabCase(Helper::singularize($item->table)),
                'routeID' => Helper::singularize($item->table),
                'relations' => implode("\n", array_map(fn ($key, $item) => $item, array_keys($tableItens['relations']), array_values($tableItens['relations']))),
                'items' => implode(",\n", array_map(fn ($item, $key) => "'$key'=>$item", array_values($tableItens['items']), array_keys($tableItens['items']))),
                'loaders' => implode(",", $tableItens['loaders'])
            ];

            // Caso não tenha loaders, remvoer as referencias
            $CODEERROR = Helper::generateCode($item->model) . '300';

            // Definição do template
            $include = __DIR__ . "/Templates/Controller/$item->model.php";
            $this->template = file_exists($include) ? include $include : include __DIR__ . "/Templates/Controller/Default.php";
            $this->template = str_replace(['//{extras}', '//{extrasUses}'], ['', ''], $this->template);

            $template = count($tableItens['loaders']) === 0
                ? str_replace(
                    [
                        "\$item->load({loaders});",
                        "\$list->with({loaders});",
                        "{joins}\n",
                        "->with({loaders})"
                    ],
                    [
                        '',
                        '',
                        '',
                        ''
                    ],
                    $this->template
                )
                : $this->template;

            $template = count($conditions) === 0
                ? str_replace(
                    ["{construct}"],
                    ["//Table not linked to a company. API access not available.\n
                    if (!\Illuminate\Support\Facades\App::runningInConsole()) {
                    throw new \\Illuminate\\Validation\\UnauthorizedException('Sorry, you don\'t have permission to access this resource. (Error. $CODEERROR)', 403);\n
                    }"],
                    $template
                )
                : $template;

            // New process to save
            SaveFile::save(self::class, $module,  $item->model, new Template($template, $update), new Template($this->templateModule, $update));
        }
        return SaveFile::countGeneratedFiles(self::class, $module);

        // return count(scandir($pathToSave)) - 2;
    }
}
