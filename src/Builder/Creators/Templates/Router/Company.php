<?php

$default = include __DIR__ . '/Default.php';

$extras = <<<EOF
Route::group([
    'middleware' => [
        'Auth.check.token',
        'Auth.check.permission',
        'Company.check.belong.to'
    ]
], function () {
    Route::get('invite/search', [App\Modules\Company\Http\Controllers\InviteController::class, 'search'])->name('invite.search');
});

Route::group([
    'middleware' => [
        'Auth.check.token',
    ]
], function () {
    Route::get('invite-me', [App\Modules\Company\Http\Controllers\InviteController::class, 'inviteMe'])->name('inviteMe.list');
    Route::put('invite-me/{id}/decide', [App\Modules\Company\Http\Controllers\InviteController::class, 'decide'])->name('inviteMe.decide');
});
EOF;

return str_replace('//{extras_up}', $extras, $default);
