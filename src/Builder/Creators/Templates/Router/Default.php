<?php

return "<?php

use Illuminate\Support\Facades\Route;
   
//{extras_up}

Route::group(['middleware' => [
    'Auth.check.token', 
    'Auth.check.permission', 
    '{module}.check.belong.to'
    ]
], function () {
    {routers}
});

//{extras_down}
";
