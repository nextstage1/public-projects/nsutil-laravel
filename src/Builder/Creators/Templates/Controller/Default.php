<?php

return "<?php

namespace App\Generated\Modules\{module}\Http\Controllers;

use App\Modules\{module}\Http\Requests\{model}Request;
use App\Modules\{module}\Http\Resources\{model}Resource;
use App\Modules\{module}\Models\{model};
use Illuminate\Http\Request;
use Closure;
//{extrasUses}

/**
 * {model}Controller
 * 
 * This class is responsible for handling requests related to the {model} model.
 * 
 * @package App\Modules\{module}\Http\Controllers
 */ 
abstract class Abstract{model}Controller extends \App\Http\Controllers\Controller
{
    /**
     * Conditions to filter by.
     *
     * @var array
     */
    protected array \$conditions = [];

    /**
     * Sortable fields.
     *
     * @var array
     */
    protected array \$sortable = [
        {sortable}
    ];

    /**
     * Searchable fields.
     *
     * @var array
     */

    protected array \$searchable = [{searchable}];

    /**
     * Searchable query.
     *
     * @var Closure
     */
    protected ?Closure \$search = null;
    

    public function __construct()
    {
        {construct}
    }

    /**
     * Initialize conditions for filtering.
     *
     * @param {model}Request \$request
     * @return void
     */
    protected function initCondition({model}Request \$request)
    {
        {conditions}

        if (null !== \$request->input('search')) {
            \$search = \$request->input('search');
            \$searchable = \$this->searchable;
            \$this->search = function (\$list) use (\$search, \$searchable) {
                foreach (\$searchable as \$index => \$field) {
                    if (\$index === 0) {
                        \$list->where(\"{table}.\$field\", 'ilike', '%' . \$search . '%');
                    } else {
                        \$list->orWhere(\"{table}.\$field\", 'ilike', '%' . \$search . '%');
                    }
                }
            };
        }

        foreach(\$this->conditions as \$key => \$val)   {
            if (null === \$val) {
                unset(\$this->conditions[\$key]);
            }
        }
    }

    /**
     * Create query to find a item or list
     *
     * @param {model}Request \$request
     * @return \Illuminate\Database\Eloquent\Builder
     */
    protected function getQuery({model}Request \$request) {
        
        \$this->initCondition(\$request);
        
        return {model}::select('{table}.*')
            {joins}
            ->where(\$this->conditions)
            ->with({loaders})
            ;
    }

    /**
     * Read a specific document type.
     *
     * @param {model}Request \$request
     * @param int \$id
     * @return {model}
     * @throws \Illuminate\Database\Eloquent\ModelNotFoundException
     */
    protected function read({model}Request \$request, int \$id) {
                    
        \$query = \$this->getQuery(\$request);
        
        return  \$query
            ->where('{table}.id', (int) \$id)
            ->firstOrFail();

    }

    /**
     * Get a list of document types.
     *
     * @param {model}Request \$request
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index({model}Request \$request)
    {
        \$list =  \$this->getQuery(\$request);

        // Limit per page. Default 30. Max 100
        \$limit = \$request->get('per_page') ?? env('LIMIT_PER_PAGE') ?? 30;

        // Order
        if (\$request->get('order')) {
            if (!in_array(\$request->get('order'), \$this->sortable)) {
                return response()->json(['error' => 'The requested field is not enabled for sorting' ], 400);
            }
            \$request->get('sort') === 'desc'
                ? \$list->orderByDesc(\$request->get('order'))
                : \$list->orderBy(\$request->get('order'));
        }

        if (null !== \$this->search) {
            \$list->where(\$this->search);
        }


        return {model}Resource::collection(
            \$list->paginate(\$limit > 100 ? 100 : \$limit)
        );
    }

    /**
     * Store a new document type.
     *
     * @param {model}Request \$request
     * @return {model}Resource
     */
    public function store({model}Request \$request)
    {
        \$this->initCondition(\$request);
        \$item = {model}::create(\$request->all());
        \$item->load({loaders});

        return (new {model}Resource(\$item))
        ->response()
        ->setStatusCode(201);
    }

    /**
     * Display the specified {model}.
     *
     * @param \App\Modules\{module}\Http\Requests\{model}Request  \$request
     * @param int  \$id
     * @return \Illuminate\Http\Response
     */
    public function show({model}Request \$request)
    {
        \$item = \$this->read(\$request, (int) \$request->route('{routeID}'));
        \$item->load({loaders});

        return new {model}Resource(\$item);
    }
    
    /**
     * Update the specified {model} in storage.
     *
     * @param \App\Modules\{module}\Http\Requests\{model}Request  \$request
     * @param int  \$id
     * @return \Illuminate\Http\Response
     */
    public function update({model}Request \$request)
    {
        \$item = \$this->read(\$request, (int) \$request->route('{routeID}'));
        \$item->update(\$request->all());
        \$item->load({loaders});

        return new {model}Resource(\$item);
    }

    /**
     * Remove the specified {model} from storage.
     *
     * @param \App\Modules\{module}\Http\Requests\{model}Request \$request
     * @param int \$id
     * @return \Illuminate\Http\Response
     */
    public function destroy({model}Request \$request)
    {
        \$item = \$this->read(\$request, (int) \$request->route('{routeID}'))
            ->delete();

        return response()->json([], 204);
    }

    //{extras}
}    
";
