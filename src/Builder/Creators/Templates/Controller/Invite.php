<?php

$default = include __DIR__ . '/Default.php';
$extras = <<<EOF
public function search(Request \$request)
{
    return new UserResource(
        User::select('*')
            ->where('email', '=', \$request->input('email'))->firstOrFail()
    );
}

public function store(InviteRequest \$request)
{
    \$profile = Profile::find(\$request->input('profile_id'));
    if (\$profile->is_root === true) {
        throw new BadRequestException(__tr('Profile root is only to owner of account'), 400);
    }

    \$this->initCondition(\$request);
    
    \$item = DB::transaction(function () use (\$request) {

        // create the invite
        \$item = Invite::create(\$request->all());

        // notify to user
        Queue::create([
            'type' => 'sendmail',
            'data' => [
                'name' => \$item->name,
                'email' => \$item->email,
                'subject' => __tr('Invite'),
                'config' => [
                    'template_id' => env('SENDGRID_INVITE'),
                    'template_data' => [
                        'name' => \$item->name,
                        'company' =>  Company::find(\$request->get('jwt_company_id'))->name
                    ]
                ]
            ]
        ]);

        return \$item;
    });

    return (new {model}Resource(\$item))
    ->response()
    ->setStatusCode(201);
}

public function decide(Request \$request, int \$id)
{
    \$invite = Invite::where('email', '=', Auth::user()->email)
        ->where('invites.id', (int) \$id)
        ->firstOrFail();

    // avaliar status
    if (\$invite->status !== 'PENDING') {
        throw new Exception(__tr('This invitation has already been decided before'), 400);
    }

    // Demais validações
    \$request->validate([
        'decision' => 'required|in:ACCEPTED,REJECTED',
        'comments' => 'nullable|string'
    ], [
        'decision.required' => 'decision: ' . __tr('The field is required'),
        'decision.in' => 'decision: ' . __tr('The values acccept are ') . 'ACCEPTED or REJECTED',
        'comments.invalid' =>  'comments: ' . __tr('Must be a text'),
    ]);

    DB::transaction(function () use (\$request, \$invite) {
        // atualizar o invite
        \$invite->update([
            'decided_at' => Carbon::now(),
            'decision_comments' => \$request->input('comments'),
            'status' => mb_strtoupper(\$request->input('decision'))
        ]);

        // gerar o user-profile
        if (\$request->input('decision') === 'ACCEPTED') {
            \$user = User::where('email', '=', \$invite->email)->firstOrFail();
            UserProfile::create([
                'profile_id' => \$invite->profile_id,
                'user_id' => \$user->id
            ]);
        }
        // notificar os interessados
        \$company = Company::select('companies.*')
            ->join('profiles', 'profiles.company_id', '=', 'companies.id')
            ->where('profiles.id', '=', \$invite->profile_id)
            ->firstOrFail();


        // notification to user
        Queue::create([
            'type' => 'sendmail',
            'data' => [
                'name' => \$user->name,
                'email' => \$user->email,
                'subject' => __tr('Decided invitation'),
                'config' => [
                    'template_id' => env('SENDGRID_INVITE_DECIDED_USER'),
                    'template_data' => [
                        'name' => \$user->name,
                        'company' => \$company->name,
                        'decision' => __tr(\$request->input('decision'))
                    ]
                ]
            ]
        ]);

        // notification to company
        Queue::create([
            'type' => 'sendmail',
            'data' => [
                'name' => \$company->name,
                'email' => \$company->email,
                'subject' => __tr('Decided invitation'),
                'config' => [
                    'template_id' => env('SENDGRID_INVITE_DECIDED_USER'),
                    'template_data' => [
                        'name' => \$company->name,
                        'invited' => \$user->name,
                        'decision' => __tr(\$request->input('decision'))
                    ]
                ]
            ]
        ]);
    });

    return new InviteResource(Invite::where('email', '=', Auth::user()->email)
    ->where('invites.id', (int) \$id)
    ->firstOrFail());
}

public function inviteMe(Request \$request)
{
    \$list = Invite::select('invites.*', 'companies.name as company_name')
        ->join('profiles', 'profiles.id', '=', 'invites.profile_id')
        ->join('companies', 'companies.id', '=', 'profiles.company_id')
        ->where('invites.email', '=', Auth::user()->email)
        ->with('profile');

    // Limit per page. Default 30. Max 100
    \$limit = \$request->get('per_page') ?? env('LIMIT_PER_PAGE') ?? 30;

    // Order
    if (\$request->get('order')) {
        if (!in_array(\$request->get('order'), \$this->sortable)) {
            return response()->json(['error' => 'The requested field is not enabled for sorting'], 400);
        }
        \$request->get('sort') === 'desc'
            ? \$list->orderByDesc(\$request->get('order'))
            : \$list->orderBy(\$request->get('order'));
    }

    if (null !== \$this->search) {
        \$list->where(\$this->search);
    }

    return InviteResource::collection(
        \$list->paginate(\$limit > 100 ? 100 : \$limit)
    );
}
EOF;

$uses = <<<EOF
use Exception;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use App\Modules\Company\Models\User;
use Illuminate\Support\Facades\Auth;
use App\Modules\Company\Models\Company;
use App\Modules\Company\Models\Profile;
use App\Modules\Application\Models\Queue;
use App\Modules\Company\Models\UserProfile;
use App\Modules\Company\Http\Resources\UserResource;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;
EOF;

$out = str_replace(
    ['public function store({model}Request', '//{extras}', '//{extrasUses}'],
    ['public function default__store({model}Request', $extras, $uses],
    $default
);


return $out;
