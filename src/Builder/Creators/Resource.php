<?php


namespace NsUtilLaravel\Builder\Creators;

use NsUtilLaravel\Builder\Data;
use NsUtilLaravel\Builder\LoadDB;
use NsUtilLaravel\Helpers\Helper;
use NsUtilLaravel\Helpers\Template;

class Resource implements CreateInterface
{

    private $template = "<?php

    namespace App\Generated\Modules\{module}\Http\Resources;
    
    use Illuminate\Http\Request;
    use Illuminate\Http\Resources\Json\JsonResource;
    
    abstract class Abstract{model}Resource extends JsonResource
    {
        /**
         * Transform the resource into an array.
         *
         * @return array<string, mixed>
         */
        public function toArray(Request \$request): array
        {
            \$data = [];
            if (\$this->resource instanceof \Illuminate\Database\Eloquent\Model) {
                {relations}
                \$data = [{items}];
            }

            return \$data;

        }
    }
    ";

    private $templateModule = "<?php

    namespace App\Modules\{module}\Http\Resources;

    use App\Generated\Modules\{module}\Http\Resources\Abstract{model}Resource;
    use Illuminate\Http\Request;
    use Illuminate\Http\Resources\Json\JsonResource;


    /**
     * Class {model}Resource
     *
     * This class extends the generated class. Update this to resolve the requirements of this application module.
     */
    final class {model}Resource extends Abstract{model}Resource
    {
        /**
         * Transform the resource into an array.
         *
         * @return array<string, mixed>
         */
        public function toArray(Request \$request): array
        {
            \$data = [];
            if (\$this->resource instanceof \Illuminate\Database\Eloquent\Model) {
                \$data = array_merge(
                    parent::toArray(\$request),
                    []
                );
            }
    
            return \$data;
        }
    }    
    ";



    public function handle(Data $data, $ignore = []): int
    {
        // $pathToSave = Helper::getPathApp() . '/app/Modules/' . $data->getData()->module . '/Http/Resources';
        // echo "- Resource: ";

        $module = $data->getData()->module;

        foreach ($data->getData()->items as $item) {
            if (array_search($item->table, $ignore) !== false) {
                continue;
            }
            $tableItens = ['relations' => [], 'items' => []];

            // Has many
            foreach ($item->hasMany as $hasMany) {
                // $tableItens['items_relation'][Helper::name2CamelCase($hasMany->table)] = HelperCreator::getNameSpace($data, $hasMany->table, '\\Http\Resources\\') . "{$hasMany->model}Resource::collection(\$this->whenLoaded('" . Helper::name2CamelCase($hasMany->table) . "'))";
            }

            foreach ($item->fields as $field) {
                // Belong to
                if (!isset($field->type)) {
                    if ($field->model !== "Company") {
                        $tableItens['items_relation'][lcfirst($field->model)] = HelperCreator::getNameSpace($data, $field->table, '\\Http\Resources\\') . "{$field->model}Resource::make(\$this->whenLoaded('" . lcfirst($field->model) . "'))";
                    }
                    continue;
                }

                if (
                    // $field->isPrimaryKey 
                    stripos($field->column_name, 'createtime') !== false
                    || stripos($field->column_name, 'created_at') !== false
                    || stripos($field->column_name, 'updated_at') !== false
                    || stripos($field->column_name, 'deleted_at') !== false
                    || stripos($field->column_name, 'password') !== false
                    || stripos($field->column_name, 'remember_token') !== false
                    || stripos($field->type, 'tsvector') !== false
                    || stripos($field->column_name, 'email_verified_at') !== false

                ) {
                    continue;
                }

                // items
                // Type
                switch (LoadDB::$config[$field->type][1]) {
                    case 'jsonb':
                    case 'json':
                        $tableItens['items'][$field->column_name] = 'json_decode($this->' . $field->column_name . ')';
                        break;
                    default:
                        $tableItens['items'][$field->column_name] = '$this->' . $field->column_name;
                }
            }

            $tableItens['relations'] = array_merge($tableItens['relations'], ($tableItens['relations_relation'] ?? []));
            $tableItens['items'] = array_merge($tableItens['items'], ($tableItens['items_relation'] ?? []));

            // Regras especificas
            if ($item->model === 'Invite') {
                $tableItens['items']['company'] = '[\'name\' => $this->company_name]';
            }

            $update = [
                'module' => $module,
                'table' => $item->table,
                'model' => $item->model,
                'relations' => implode("\n", array_map(fn ($key, $item) => $item, array_keys($tableItens['relations']), array_values($tableItens['relations']))),
                'items' => implode(",\n", array_map(fn ($item, $key) => "'$key'=>$item", array_values($tableItens['items']), array_keys($tableItens['items']))),
            ];

            SaveFile::save(self::class, $module,  $item->model, new Template($this->template, $update), new Template($this->templateModule, $update));

            // (new Template($this->template, $update))
            //     ->renderTo($pathToSave . '/' . $item->model . 'Resource.php', Helper::IF_NOT_UPDATE);
        }

        // return count(scandir($pathToSave)) - 2;
        return SaveFile::countGeneratedFiles(self::class, $module);
    }
}
