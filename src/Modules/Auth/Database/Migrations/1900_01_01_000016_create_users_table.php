<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->softDeletes();
            $table->string('name', '255')->comments('Name');
            $table->string('email', '255')->comments('Email');
            $table->timestamp('email_verified_at')->nullable()->comments('Email verified at');
            $table->string('password', '255')->comments('Password');
            $table->unique(['email']);
        });
    }

    public function down()
    {
        Schema::dropIfExists('users');
    }
};
