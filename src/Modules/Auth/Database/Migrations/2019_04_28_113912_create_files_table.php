<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('files', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->string('filename', 50);
            $table->string('name', 100)->nullable();
            $table->string('description', 250)->nullable();
            $table->string('origin', 150)->nullable();
            $table->string('extension', 5)->nullable();
            $table->string('mime', 50)->nullable();
            $table->integer('classification')->default(1);
            $table->string('recognition', 50)->nullable();
            $table->text('content')->nullable();
            $table->string('storage', 30)->default('Local');
            $table->foreignId('company_id')->constrained('companies')->onUpdate('cascade')->onDelete('cascade');

            $table->unique(['filename', 'company_id']);
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('files');
    }
};
