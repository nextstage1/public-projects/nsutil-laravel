<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    public function up()
    {
        Schema::create('permissions', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->string('group', '50')->comments('Group');
            $table->string('route', '50')->comments('Route');
            $table->string('verb', '50')->comments('Verb');
            $table->unique(['route', 'verb']);
        });
    }

    public function down()
    {
        Schema::dropIfExists('permissions');
    }
};
