<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('queues', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->timestamp('executed_at')->nullable();
            $table->string('type', '50');
            $table->enum('status', ['PENDING', 'RUNNING', 'SUCCESS', 'ERROR'])->default('PENDING')->nullable(false);
            $table->boolean('remove_on_success')->default(true);
            $table->jsonb('data');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('queues');
    }
};
