<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('invites', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->string('email', 200);
            $table->string('name', 200);
            $table->date('decided_at')->nullable()->comment('Date of invite decision');
            $table->date('expire_at')->default('2100-12-31')->comment('Expiration date of invite');
            $table->text('sent_comments')->nullable();
            $table->text('decision_comments')->nullable();
            $table->enum('status', ['PENDING', 'ACCEPTED', 'REJECTED', 'EXPIRED'])->default('PENDING');
            $table->foreignId('profile_id')->constrained('profiles')->onUpdate('cascade')->onDelete('cascade');

            $table->unique(['email', 'profile_id', 'status']);
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('invites');
    }
};
