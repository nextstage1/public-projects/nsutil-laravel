<?php

use Illuminate\Support\Facades\Route;
use NsUtilLaravel\Modules\Auth\Http\Controllers\AuthController;
use NsUtilLaravel\Modules\Auth\Http\Controllers\InviteReceivedController;
use NsUtilLaravel\Modules\Auth\Http\Controllers\InviteSentController;
use NsUtilLaravel\Modules\Auth\Http\Controllers\PasswordResetLinkController;
use NsUtilLaravel\Modules\Auth\Http\Controllers\RegisterUserController;
use NsUtilLaravel\Modules\Auth\Http\Controllers\SocialAuthController;

$limitSeconds = env('APP_ENV') === 'testing' ? 120 : 10;
// Auth modules
Route::middleware(["throttle:{$limitSeconds},1"])->group(function () {

    Route::get('healthcheck', fn () => response()->json('healthy', 200))->name('app.healthcheck');

    // Login 
    Route::prefix('auth')
        ->group(function () {
            Route::post('login', [AuthController::class, 'login'])->name('auth.login');
            Route::post('logout', [AuthController::class, 'logout'])->name('auth.logout');
            Route::post('forgot-password', [PasswordResetLinkController::class, 'forgot'])->name('auth.forgot');
            Route::post('reset-password', [PasswordResetLinkController::class, 'reset'])->name('auth.reset');

            // Google
            Route::get('google', [SocialAuthController::class, 'redirectToGoogle'])->name('auth.google');
            Route::get('google/callback', [SocialAuthController::class, 'handleGoogleCallback'])->name('auth.google.callback');

            // Renew token
            Route::group(['middleware' => ['Auth.check.token']], function () {
                Route::post('renew', [AuthController::class, 'tokenRenew'])->name('auth.renew');
            });
        });

    // Create account
    Route::prefix('signup')
        ->group(function () {
            Route::post('', [RegisterUserController::class, 'register'])->name('signup.register');
            Route::post('resend', [RegisterUserController::class, 'resendToken'])->name('signup.resend');
            Route::post('confirm', [RegisterUserController::class, 'confirm'])->name('signup.confirm');
        });
});
