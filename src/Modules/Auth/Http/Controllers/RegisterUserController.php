<?php

namespace NsUtilLaravel\Modules\Auth\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Modules\Application\Models\Queue;
use App\Modules\Company\Models\User;
use Illuminate\Support\Facades\Hash;
use App\Modules\Company\Models\Company;
use App\Modules\Company\Models\Profile;
use App\Modules\Company\Models\UserProfile;
use NsUtilLaravel\Contracts\MailServiceInterface;
use NsUtilLaravel\Modules\Auth\Http\Requests\RegisterRequest;
use NsUtilLaravel\Modules\Auth\Http\Requests\ResetRequest;

class RegisterUserController extends Controller
{


    public function __construct()
    {
    }

    private function sendToken(string $email, string $name, string $subject = 'Confirm your account')
    {
        // Enviar email do confirmação
        DB::table('password_reset_tokens')->where('email', $email)->delete();
        $token = rand(100000, 999999);
        DB::table('password_reset_tokens')->insert([
            'email' => $email,
            'token' => (string) $token,
            'created_at' => Carbon::now()
        ]);

        // notification to company
        return Queue::create([
            'type' => 'sendmail',
            'data' => [
                'name' => $name,
                'email' => $email,
                'subject' => $subject,
                'config' => [
                    'template_id' => env('SENDGRID_SIGNUP_CODE'),
                    'template_data' => ['name' => $name, 'code' => $token]
                ]
            ]
        ]);
    }

    // registra um usuario
    public function register(RegisterRequest $request)
    {

        $ret = DB::transaction(function () use ($request) {
            $itens = $request->only('name', 'email');
            $itens['password'] = 'PENDING';
            $user = User::create($itens);
            return $this->sendToken($user->email, $user->name);
        });

        return response()->json(
            $ret->id > 0
                ? ['message' => 'User created! Check your e-mail to validate your account']
                : ['error' => __tr('Error on send code')],
            $ret->id > 0 ? 200 : 400
        );
    }

    public function resendToken(Request $request)
    {

        $validatedData = $request->validate([
            'email' => 'required|email|exists:users,email'
        ]);

        $user = User::where('email', '=', (string)$request->email)
            ->firstOrFail();

        if (!$user) {
            throw new \InvalidArgumentException('Account not found', 404);
        }

        if ($user->email_verified_at !== null) {
            throw new \InvalidArgumentException('Account has already been verified', 422);
        }

        $ret = $this->sendToken($user->email, $user->name);

        return response()->json(
            $ret->id > 0
                ? ['message' => 'Token resent']
                : ['error' => __tr('Error on send code')],
            $ret->id > 0 ? 200 : 400
        );
    }

    // valida o código enviado por email
    public function confirm(ResetRequest $request)
    {
        // Encontrar o usuário com o email fornecido
        $user = User::where('email', $request->email)->firstOrFail();

        DB::transaction(function () use ($request, $user) {

            // Criar empresa
            $company = $this->createUserCompany($user);

            // Criar profile
            $this->createProfileRoot($user, $company);

            // Atualizar a senha do usuário
            $user->password = Hash::make($request->password);
            $user->email_verified_at = Carbon::now();
            $user->save();

            // Remover o token utilizado
            DB::table('password_reset_tokens')->where('email', $request->email)->delete();

            Queue::create([
                'type' => 'sendmail',
                'data' => [
                    'name' => $user->name,
                    'email' => $user->email,
                    'subject' => __tr('Account actived'),
                    'config' => [
                        'template_id' => env('SENDGRID_SIGNUP_ACCOUNT_CREATED'),
                        'template_data' => ['name' => $user->name, 'plan_name' => 'FREE']
                    ]
                ]
            ]);
        });

        return response()->json(['message' => 'Account activated!'], 200);
    }

    // Cria a empresa e associa ao usuario
    public function createUserCompany(User $user)
    {
        return Company::create([
            'name' => $user->name,
            'email' => $user->email
        ]);
    }

    // Cria o profile administrador com permissão root
    public function createProfileRoot(User $user, Company $company)
    {
        $profile = Profile::create([
            'name' => 'Administrator',
            'company_id' => $company->id,
            'is_root' => true
        ]);

        UserProfile::create([
            'user_id' => $user->id,
            'profile_id' => $profile->id
        ]);

        return $profile;
    }
}
