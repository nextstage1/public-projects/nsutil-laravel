<?php

namespace NsUtilLaravel\Modules\Auth\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Tymon\JWTAuth\Facades\JWTAuth;
use App\Http\Controllers\Controller;
use App\Modules\Company\Models\User;
use Laravel\Socialite\Facades\Socialite;
use App\Auth\Http\Requests\LoginRequest;
use App\Auth\Http\Requests\ResetRequest;
use NsUtilLaravel\Services\SendGridMailService;

class SocialAuthController extends Controller
{
    public function redirectToGoogle()
    {
        $redirectResponse = Socialite::driver('google')
            ->stateless()
            ->setScopes(['email', 'profile'])
            ->redirectUrl(route('auth.google.callback'))
            ->redirect();

        $authorizationUrl = $redirectResponse->getTargetUrl();

        return response()->json(['authorization_url' => $authorizationUrl]);
    }

    public function handleGoogleCallback(Request $request)
    {
        $mailService = app()->make(MailServiceInterface::class);
        try {
            $googleUser = Socialite::driver('google')->stateless()->user();
        } catch (\Exception $e) {
            return response()->json(['error' => 'An error occurred while authenticating with Google. Please try again.'], 400);
        }

        $user = User::first(['email' => $googleUser->getEmail()]);
        if (!$user) {
            // Gerar token fake para validar
            DB::table('password_reset_tokens')->where('email', $googleUser->getEmail())->delete();
            $token = 'Login by Google';
            DB::table('password_reset_tokens')->insert([
                'email' => $googleUser->getEmail(),
                'token' => (string) $token,
                'created_at' => Carbon::now()
            ]);

            // Criar e validar usuario
            $itens['password'] = 'PENDING';
            $user = User::create([
                'password' => 'GOOGLE',
                'name' => $googleUser->getName(),
                'email' => $googleUser->getEmail(),
                'email_verified_at' => Carbon::now()
            ]);

            // Confirmar a conta
            $request = new ResetRequest();
            $request->merge([
                [
                    "email" => $googleUser->getEmail(),
                    "token" => $token,
                    "password" => "PENDING-BY-GOOGLE",
                    "password_confirmation" => "PENDING-BY-GOOGLE"
                ]
            ]);
            $ret = (new RegisterUserController($mailService))->confirm($request);
        }

        // Gerar token JWT e retornar
        $token = JWTAuth::fromUser($user);
        $request = new LoginRequest();
        $request->merge(['token_social' => $token]);
        return (new AuthController($mailService))
            ->login($request);
    }
}
