<?php

namespace NsUtilLaravel\Modules\Auth\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Modules\Application\Models\Queue;
use App\Modules\Company\Models\User;
use Illuminate\Support\Facades\Hash;
use NsUtilLaravel\Contracts\MailServiceInterface;
use NsUtilLaravel\Modules\Auth\Http\Requests\ForgotRequest;
use NsUtilLaravel\Modules\Auth\Http\Requests\ResetRequest;

class PasswordResetLinkController extends Controller
{

    public function __construct()
    {
    }
    /**
     * Handle an incoming password reset link request.
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    public function forgot(ForgotRequest $request): JsonResponse
    {
        $email = $request->input('email');
        $status = 400;
        $message = '';

        $user = User::where('email', $email)->firstOrFail();

        // Gerar token
        $token = fake()->numberBetween(100000, 999999);
        DB::table('password_reset_tokens')->where('email', $email)->delete();
        DB::table('password_reset_tokens')->insert([
            'email' => $email,
            'token' => (string) $token,
            'created_at' => Carbon::now()
        ]);



        // Enviar email
        Queue::create([
            'type' => 'sendmail',
            'data' => [
                'name' => $user->name,
                'email' => $user->email,
                'subject' => __tr('Password recovery request'),
                'config' => [
                    'template_id' => env('SENDGRID_FORGOT_PASSWORD'),
                    'template_data' => ['name' => $user->name, 'code' => $request->token]
                ]
            ]
        ]);

        return response()->json(['message' => __tr('Token for password recovery sent. Check email.')]);
    }

    public function reset(ResetRequest $request)
    {
        // Encontrar o usuário com o email fornecido
        $user = User::where('email', $request->email)->firstOrFail();

        // Atualizar a senha do usuário
        $user->password = Hash::make($request->password);
        $user->save();

        // Remover o token utilizado
        DB::table('password_reset_tokens')->where('email', $request->email)->delete();

        // Enviar email informando alteração de senha
        Queue::create([
            'type' => 'sendmail',
            'data' => [
                'name' => $user->name,
                'email' => $user->email,
                'subject' => __tr('Password changed'),
                'config' => [
                    'template_id' => env('SENDGRID_FORGOT_PASSWORD'),
                    'template_data' => ['name' => $user->name]
                ]
            ]
        ]);

        return response()->json(['message' => 'Password successfully updated'], 200);
    }
}
